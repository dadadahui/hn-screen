$.divselect('#select-year', '#input-year')

$('#radio-select input').on('click',function(e){
  if ($('#radio-year').attr('checked')){
    $('#select-year').show().siblings('.select-month').hide();
    // $.divselect('#select-year', '#input-year')
  }else if ($('#radio-month').attr('checked')){
    $('#select-month').show().siblings('.select-year').hide();
    $("#month-begin").simpleCanleder();

    $("#month-end").simpleCanleder();
  }
})

var yearSelected,
  currYear = new Date().getFullYear()

$('#input-year').val(currYear)
//监听下拉
$('#select-year a').on('click', function (e) {
  yearSelected = $('#input-year').val()
  getDataByYear(yearSelected)
})

getDataByYear(currYear)

function getDataByYear (yearSelected) {
  var pie1Data = [
    {value: 51, name: '年度检查'},
    {value: 81, name: '月度检查'},
    {value: 15, name: '日常检查'},
    {value: 10, name: '事故调查'},
    {value: 10, name: '投诉举报检查'}
  ]

  var bar1data = {
    x: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
    series: [
      {
        name: '计划检查企业数',
        data: [10, 52, 66, 334, 390, 330, 220]
      },
      {
        name: '已检查企业数',
        data: [10, 52, 200, 334, 77, 330, 220]
      },
      {
        name: '完成率',
        data: [10, 66, 33, 10, 66, 3]
      }
    ]

  }
  var bar2data = {
    x: ['郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局', '郑州市安监局'],
    series: [
      {
        name: '计划检查企业数',
        data: [10, 52, 66, 334, 390, 330, 220]
      },
      {
        name: '已检查企业数',
        data: [10, 52, 200, 334, 77, 330, 220]
      },
      {
        name: '完成率',
        data: [10, 66, 33, 10, 66, 3]
      }
    ]

  }
  var bar3data = {
    y: ['危险化学品企业', '烟花爆竹企业', '非煤矿山企业', '工贸八大行业企业'],
    series: [10, 52, 66, 334],
    series1: [55, 100, 22, 110]
  }

  var top5data = {
    cfcs: [
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      }
    ],
    cfje: [
      {
        unit: '郑州市安监局',
        data: '1111次'
      },
      {
        unit: '郑州市安监局',
        data: '11122次'
      },
      {
        unit: '郑州市安监局',
        data: '11331次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      }
    ],
    jccs: [
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '222次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      }
    ],
    pcyhs: [
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      }
    ],
    las: [
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      },
      {
        unit: '郑州市安监局',
        data: '111次'
      }
    ]
  }
  var mapData = [
    {
      'name': '漯河市',
      'value': 211,
      'code': '411100'
    },
    {
      'name': '洛阳市',
      'value': 1000,
      'code': '410300'
    },
    {
      'name': '开封市',
      'value': 0,
      'code': '410200'
    }
  ]
  var mapmax = 1000
var mapData1 = [
  {
    name: "吉利区",
    value: 22,
    code: "1"
  },
  {
    name: "新安县",
    value: 200,
    code: "2"
  }
];
  //地图面板展示数据
  var boardData = {
    planCheck: 218,
    haschecked: 171,
    totalCheckTimes: 1111
  }

  function refreshBoard () {
    $('#planCheck').text(boardData.planCheck)
    $('#haschecked').text(boardData.haschecked)
    $('#rate').text(toFixed2(boardData.haschecked / boardData.planCheck))
    $('#totalCheckTimes').text(boardData.totalCheckTimes)
  }
  refreshBoard()
  // 测试
  if (yearSelected == 2017) {

    var pie1Data = [
      {value: 100, name: '年度检查'},
      {value: 811, name: '月度检查'},
      {value: 15, name: '日常检查'},
      {value: 10, name: '事故调查'},
      {value: 1110, name: '投诉举报检查'}
    ]
  }

  var pie1 = showPie1(pie1Data)
  var bar1 = showBar1(bar1data)
  var bar2 = showBar2(bar2data)
  var bar3 = showBar3(bar3data)

  showTop5(top5data)
  initTop5(top5data)

  var map = showMap(mapData, mapmax, '41',function(chart){
  
    chart.on('click', function (series) {
      var name = series.data!== undefined ? series.data.name :undefined
      var code = series.data!== undefined ? series.data.code :undefined
      if (code ==  '410100' || code == '410200' || code =='410300' || code =='410400' || code =='410500' || code =='410600' || code =='410700'
        || code =='410800' || code =='410900' || code =='411100' || code =='411200' ||code =='411300' || code =='411400' ||code =='411500' ||
        code =='411600' ||code =='411700' ||code =='411900'){
        $('#currCity').text(name).show()
        showMap(mapData1, mapmax, code)
      }else if(code == 'undefined'){
        return
      }

    })
  })



  //返回省级
  $('#province').on('click', function (e) {
    showMap(mapData, mapmax, '41')
    $('#currCity').hide()
  })

}

function initTop5 (top5data) {
  var htmlStr = ''
  var dataArr = top5data.jccs
  for (var i = 0; i < dataArr.length; i++) {
    var dataItem = dataArr[i]
    htmlStr += '    <tr>' +
      '              <th>' + (i + 1) + '</th>' +
      '              <td>' + dataItem.unit + '</td>' +
      '              <td>' + dataItem.data + '</td>' +
      '            </tr>'
  }
  $('#grid-body').html(htmlStr)
}
