//行业类别
function getColorByCodeOrName(code,name) {
  if (code == "SYS1601" || name == '危险化学品') {  // 危险化学品
    return "#FF4321";
  } else if (code == "SYS1602" || name == '烟花爆竹') {//烟花爆竹
    return "#B91D1C";
  } else if (code == "SYS1603" || name == '非煤矿山') {//非煤矿山
    return "#5C5F6B";
  } else if (code == "SYS1604" || name == '冶金行业') {//冶金行业
    return "#F99A1B";
  } else if (code == "SYS1605" || name == '有色金属') {//有色金属
    return "#7FA51D";
  } else if (code == "SYS1606" || name == '建材行业') {//建材行业
    return "#139AC2";
  } else if (code == "SYS1607" || name == '机械制造') {//机械制造
    return "#005D93";
  } else if (code == "SYS1608" || name == '轻工业') {//轻工业
    return "#48A325";
  } else if (code == "SYS1609" || name == '纺织工业') {//纺织工业
    return "#ED5A0D";
  } else if (code == "SYS1610" || name == '烟草行业') {//烟草行业
    return "#006C26";
  }  else if (code == "SYS1611" || name == '商贸行业') {//商贸行业
    return "#27962E";
  } else if(code == "SYS1699" || name == '其他') {//其他
    return "#5C5F6B";
  }else{
    return '#018DD3'
  }
}

function getVisualMapColorByCodeOrName (code, name) {
  if (code == "SYS1601" || name == '危险化学品') {  // 危险化学品
    return ["#E93414","#FCDBD1"];
  } else if (code == "SYS1602" || name == '烟花爆竹') {//烟花爆竹
    return ["#B91D1C","#F4D6D3"];
  } else if (code == "SYS1603" || name == '非煤矿山') {//非煤矿山
    return ["#474646","#D2D2D2"];
  } else if (code == "SYS1604" || name == '冶金行业') {//冶金行业
    return ["#F99A1B","#FEEED3"];

  } else if (code == "SYS1605" || name == '有色金属') {//有色金属
    return ["#7FA51D","#E9EFD3"];

  } else if (code == "SYS1606" || name == '建材行业') {//建材行业
    return ["#139AC2","#CCEEF5"];

  } else if (code == "SYS1607" || name == '机械制造') {//机械制造
    return ["#005D93","#CCE3ED"];

  } else if (code == "SYS1608" || name == '轻工业') {//轻工业
    return ["#48A325","#DDEFD6"];

  } else if (code == "SYS1609" || name == '纺织工业') {//纺织工业
    return ["#ED5A0D","#FDE2CE"];
  } else if (code == "SYS1610" || name == '烟草行业') {//烟草行业
    return ["#006C26","#D5EDD8"];
  }  else if (code == "SYS1611" || name == '商贸行业') {//商贸行业
    return ["#27972E","#D5EDD8"];
  } else if(code == "SYS1699" || name == '其他') {//其他
    return ["#5C5F6B","#E2E3E5"];
  } else{
    return  [ '#284CA9', '#4DD5EF']
  }
}

// 企业行业分布情况
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '企业行业分布情况',
          type: 'pie',
          radius: [25, 45],
          center: ['50%', '65%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine:{
            normal:{
              length:2,
              length1:2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color: function (value) {
                return getColorByCodeOrName(value.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart;
}
function showBar1 (data,pcode) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {

      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '10%',
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom: '8',
          startValue: 0,
          endValue: 4,
          // end: 70,
          height: '22',
          backgroundColor: 'rgba(9,75,127,0.2)',
          fillerColor: '#094B7F',
          borderColor: '#094B80',
          textStyle: {
            color: '#97B8C9'
          }
        }

      ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            },
            formatter: function (val) {
              return wrapByNum(val)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '行政许可持证情况',
          type: 'bar',
          barWidth: '50%',
          data: data.data,
          itemStyle:{
            normal:{
              color:function(){
                return getColorByCodeOrName(pcode)
              }
            }
          }
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

// 企业监管特性分布
function showBar2 (data) {
  var chart = echarts.init(document.getElementById('bar2'))
  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      color:'#6A21E5',
      grid: {
        top: '25%',
        left: '3%',
        right: '5%',
        bottom: '5%',
        containLabel: true
      },

      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            },
            formatter:function (val) {
              return wrapByNum(val)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false
          }
        }
      ],
      series: [
        {
          name: '企业监管特性分布',
          type: 'bar',
          barWidth: '50%',
          yAxisIndex: 0,
          data: data.data,

          label:{
            normal:{
              show:false,
              color:'#97B8C9',
            }
          },
          itemStyle:{
            normal:{
              color:'#6A21E5',
            }
          }
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}


// 各地区重点监管企业分布情况
function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'))
  var option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {            // 坐标轴指示器，坐标轴触发有效
        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
      }
    },
    dataZoom: [
      {
        show: true,
        bottom: '8',
        start: 0,
        end: 70,
        height: '22',
        backgroundColor: 'rgba(9,75,127,0.2)',
        fillerColor: '#094B7F',
        borderColor: '#094B80',
        textStyle: {
          color: '#97B8C9'
        }
      }

    ],
    grid: {
      top: '35%',
      left: '6%',
      right: '5%',
      bottom: '15%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      axisLabel: {
        textStyle: {
          color: '#97B8C9'
        }
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#07274D'
        }
      },
      axisTick: {
        show: true,
        lineStyle: {
          color: '#1178C9'
        }
      },

      data: data.x
    },
    yAxis: [
      {
        type: 'value',
        splitLine: {
          show: true,
          lineStyle: {
            color: '#07274D'
          }
        },
        axisLine: {

          show: true,

          lineStyle: {
            color: '#07274D'
          }
        },
        axisTick: {
          show: true,
          lineStyle: {
            color: '#1178C9'
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#97B8C9'

          }

        }
      }

    ],
    series: {
      name: '企业个数',
      type: 'bar',
      itemStyle: {
        normal: {
          color:'#FF7E22'
        }
      },
      data: data.data

    }
  }

  chart.setOption(option)
  $(window).resize(function () {
      chart.resize()
    }
  )
}

function showMap (mapdata, max, code,callback) {
  var chart = echarts.init(document.getElementById('map'))

  function getSerieTotal (data) {
    var total = 0
    for (var i = 0; i < data.length; i++) {
      total += data[i].value
    }
    return total
  }

  $.get('./map/' + code + '.json', {async:false},function (json) {

    echarts.registerMap(code, json)
    var option = {
      tooltip: {
        trigger: 'item'
      },
      legend: {
        selectedMode: 'single',
        orient: 'vertical',
        itemWidth: 12,
        itemHeight: 12,
        x: '20',
        y: '70',
        icon: 'circle',
        data: [],
      },
      visualMap: {
        min: 0,
        max: max,
        right: '20',
        bottom: '18',
        textStyle: {
          color: '#4BCEEC'
        },
        text: ['高', '低'],           // 文本，默认为数值文本
        color: [
          '#284CA9',
          '#4DD5EF'],
        calculable: true
      },
      series: [
        {
          name: '生产经营单位总数',
          roam: true,
          type: 'map',
          zoom: 1.2,
          mapType: code,
          label: {
            normal: {
              show: true,
              color: '#062E4C'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1.5
            }
          },
          data: mapdata
        }
      ]
    }
    for (var i = 0; i < mapdata.length; i++) {
      var serie = {
        name: function () {
          var data = mapdata[i].data
          return mapdata[i].type + '(' + getSerieTotal(data) + ')'
        }(),
        roam: true,
        type: 'map',
        zoom: 1.2,
        left: '200',
        map: code,
        label: {
          normal: {
            show: true,
            color: '#006ACC'
          }
        },
        itemStyle: {
          normal: {
            borderColor: '#64B6FE',
            borderWidth: 1.5,
            areaColor: '#D1F1FF',
            shadowOffsetX: 4,
            shadowOffsetY: 4,
            shadowColor: '#50708d',
            shadowBlur: 20,
            color: getColorByCodeOrName(mapdata[i].code)
          }
        },
        data: mapdata[i].data
      }
      option.series.push(serie)
      if (i == 0) {
        option.legend.data.push({
          name: mapdata[i].type + '(' + getSerieTotal(option.series[1].data) + ')',
          textStyle: {
            color: '#018DD3'
          }
        })
      } else {
        option.legend.data.push({
          name: mapdata[i].type + '(' + getSerieTotal(option.series[i + 1].data) + ')',
          textStyle: {
            color: getColorByCodeOrName(mapdata[i].code)
          }
        })
      }
    }

    var legendData = option.legend.data

    chart.setOption(option)

    if (callback){
      callback(chart);
    }else{
      return
    }
  })
}

//企业分级情况
function showBar4 (data) {

  var chart = echarts.init(document.getElementById('bar4'))
  var option = {
    baseOption: {
      animationDurationUpdate: 1000,
      animationEasingUpdate: 'quinticInOut',
      timeline: {
        axisType: 'category',
        left: '25',
        right: '25',
        top:'40',
        autoPlay: true,
        playInterval: 15000,
        label: {
          normal: {
            textStyle: {
              color: '#0D80FE',
              padding: 10
            },
            position: 'bottom'
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false
        },
        data: ['企业规模', '标准化等级', '诚信等级', '风险等级']
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      grid:{
        top:'45%',
        bottom:'15%',
        left:'15%'
      },
      series:[{
        type: 'bar',
        itemStyle: {
          normal: {
            color: function(value) {
              var colorList = [
                '#05D187',
                '#FF7200',
                '#FAB034',
                '#29A1F7'
              ]
              return colorList[value.dataIndex]

            }
          }
        },
      }]
    },
    options: [{
      series:[
        {
          name: '企业规模',
          type: 'bar',
          barWidth: '50%',
          data: data.qygm.data
        }
      ],
      xAxis: [
        {
          data: data.qygm.x
        }
      ]
    },
      {
      series:[
        {
          name: '标准化等级',
          type: 'bar',
          barWidth: '50%',
          data: data.bzhdj.data
        }
      ],
      xAxis: [
        {
          data: data.bzhdj.x
        }
      ]
    },
      {
        series:[
          {
            name: '诚信等级',
            type: 'bar',
            barWidth: '50%',
            data: data.cxdj.data
          }
        ],
        xAxis: [
          {
            data: data.cxdj.x
          }
        ]
      },
      {
        series:[
          {
            name: '风险等级',
            type: 'bar',
            barWidth: '50%',
            data: data.fxdj.data
          }
        ],
        xAxis: [
          {
            data: data.fxdj.x
          }
        ]
      }
    ]
  }

  chart.setOption(option)




}