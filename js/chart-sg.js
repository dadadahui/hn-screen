//事故等级
function getColorByCodeOrName(code,name) {
  if (code == "E9001" || name == '特别重大事故') {
    return "#C1232B";
  } else if (code == "E9002" || name == '重大事故') {
    return "#FF7E22";
  } else if (code == "E9003" || name == '较大事故') {
    return "#FCCE27";
  } else if (code == "E9004" || name == '一般事故') {
    return "#21D376";
  }else{
    return '#C1232B'
  }
}

function getVisualMapColorByCodeOrName (code, name) {
  if (code == "SYS1601" || name == '危险化学品') {  // 危险化学品
    return ["#E93414","#FCDBD1"];
  } else if (code == "SYS1602" || name == '烟花爆竹') {//烟花爆竹
    return ["#B91D1C","#F4D6D3"];
  } else if (code == "SYS1603" || name == '非煤矿山') {//非煤矿山
    return ["#474646","#D2D2D2"];
  } else if (code == "SYS1604" || name == '冶金行业') {//冶金行业
    return ["#F99A1B","#FEEED3"];

  } else if (code == "SYS1605" || name == '有色金属') {//有色金属
    return ["#7FA51D","#E9EFD3"];

  } else if (code == "SYS1606" || name == '建材行业') {//建材行业
    return ["#139AC2","#CCEEF5"];

  } else if (code == "SYS1607" || name == '机械制造') {//机械制造
    return ["#005D93","#CCE3ED"];

  } else if (code == "SYS1608" || name == '轻工业') {//轻工业
    return ["#48A325","#DDEFD6"];

  } else if (code == "SYS1609" || name == '纺织工业') {//纺织工业
    return ["#ED5A0D","#FDE2CE"];
  } else if (code == "SYS1610" || name == '烟草行业') {//烟草行业
    return ["#006C26","#D5EDD8"];
  }  else if (code == "SYS1611" || name == '商贸行业') {//商贸行业
    return ["#27972E","#FCDBD1"];
  } else if(code == "SYS1699" || name == '其他') {//其他
    return ["#5C5F6B","#E2E3E5"];
  } else{
    return  [ '#284CA9', '#4DD5EF']
  }
}

// 年度最易发生事故类型top10
function showBar1 (data) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'none'
        },
        formatter: function (params) {
          return params[0].name;
        }
      },
      grid:{
        left:'5%',
        top:'18%',
        bottom:'3%',
        containLabel: true
      },
      yAxis: {
        data: data.y,
        inverse:true,
        axisTick: {show: false},
        axisLine: {show: false},
        axisLabel: {
          interval:0,
          textStyle: {
            color: '#BBF3FF'
          },
          formatter:function(val){
            return wrapByNum1(val)
          }
        },

      },
      xAxis: {
        splitLine: {show: false},
        axisTick: {show: false},
        axisLine: {show: false},
        axisLabel: {show: false}
      },
      color: ['#0099FF'],
      series: [{
        name: 'hill',
        type: 'pictorialBar',
        barCategoryGap: '-130%',
        symbol: 'path://M6,0 C13.336761,21.3333333 60.0034276,32 146,32 C59.9651924,32 13.2985257,42.6666667 6,64 L6,0 Z',

        itemStyle: {
          normal: {
            opacity: 0.5
          },
          emphasis: {
            opacity: 1
          }
        },
        data:data.data,
        z: 10
      },

      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}
// 事故等级分析
function showBar2 (data) {
  var chart = echarts.init(document.getElementById('bar2'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
      },
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['特大', '重大', '较大','一般'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '特大',
          type: 'bar',
          stack:1,
          itemStyle:{
            normal:{
              color:'#C1232B'
            }
          },
          data: data.data[0]
        },
        {
          name: '重大',
          type: 'bar',
          stack:1,
          itemStyle:{
            normal:{
              color:'#FF7E22'
            }
          },
          data: data.data[1]
        },
        {
          name: '较大',
          type: 'bar',
          stack:1,
          itemStyle:{
            normal:{
              color:'#FCCE27'
            }
          },
          data: data.data[2]
        },
        {
          name: '一般',
          type: 'bar',
          stack:1,
          itemStyle:{
            normal:{
              color:'#21D376'
            }
          },
          data: data.data[3]
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

// 事故原因分析
function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '5%',
        containLabel: true
      },

      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            },
            formatter:function(val){
              return wrapByNum(val)
            }

          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          }
        }
      ],
      series: [
        {
          name: '',
          type: 'bar',
          barWidth:'55%',
          itemStyle:{
            normal:{
              color:'#21D376'
            }
          },
          data: data.data
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

// 事故情况分析
function showBarRadius() {
  var myChart = echarts.init(document.getElementById('bar-radius'));
  var option = {
    tooltip: {
      trigger: 'item'
    },

    angleAxis: {
      axisLabel: {
        color: '#97B8C9',
        margin: 2
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.2)'
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: ['rgba(16,129,208,.2)']
        }
      },
      axisTick: {
        show: false
      }
    },
    radiusAxis: {
      show: false,
      // nameLocation: 'start',
      // nameGap: 2,
      type: 'category',
      splitArea: {
        show: true,
        areaStyle: {
          color: 'rgba(16,129,208,.2)'
        }
      },

      data: [
        '危险化学品',
        '烟花爆竹',
        '非煤矿山',
        '冶金行业',
        '有色金属',
        '建材行业',
        '机械制造',
        '轻工业',
        '纺织工业',
        '烟草行业',
        '商贸行业',
        '其他'
      ],
      z: 30
    },
    color: [
      '#BBF3FF',
      '#1178C9',
      '#4CAF50',
      '#8B572A',
      '#F8E71C',
      '#73BCF0',
      '#DC1F7A',
      '#EB3432',
      '#F5A623',
      '#7630ED',
      '#3157EC',
      '#6ECFE6',
      '#971F2E',
      '#9A911A',
      '#7ED321',
      '#5A3D23',
      '#563491',
      '#91949C',
      '#28335F',
      '#B09A75'
    ],
    polar: {
        radius:'60%',
        center: ['63%', '55%']
    },
    series: [
      {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
       name: '物体打击',
        stack: 'a',
        radius: [
          '0%', '10%'
        ],
        center: [
          '63%', '55%'
        ],

      },
      {
        type: 'bar',
        data: [
          0,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        name: '机械伤害',
        center: [
          '63%', '55%'
        ],
        stack: 'a'
      },
      {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
       name: '起重伤害',

        center: [
          '63%', '55%'
        ],
        stack: 'a'
      }, {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12,
          13
        ],
        coordinateSystem: 'polar',
        center: [
          '63%', '55%'
        ],

        name: '淹溺',
        stack: 'a'
      }, {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        center: [
          '63%', '55%'
        ],

        name: '灼烫',
        stack: 'a'
      },
      {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        center: [
          '63%', '55%'
        ],

        name: '火灾',
        stack: 'a'
      },
    ],
    legend: {
      show: true,
      type: 'scroll',
      orient: 'vertical',
      top:'50px',
      left: '26px',
      pageIconColor: '#fff',
      pageIconInactiveColor: '#ACCAD0',
      pageTextStyle: {
        color: '#ACCAD0'
      },
      itemWidth: 14,
      itemHeight: 8,
      textStyle: {
        color: '#97B8C9',
        fontSize: 12
      },
      data: [
        '物体打击',
        '车体伤害',
        '机械伤害',
        '起重伤害',
        '触电',
        '淹溺',
        '灼烫',
        '火灾',
        '高处坠落',
        '坍塌',
        '冒顶片帮',
        '透水',
        '放炮',
        '火药爆炸',
        '瓦斯爆炸',
        '锅炉爆炸',
        '容器爆炸',
        '其他爆炸',
        '中毒和窒息',
        '其他伤害'
      ]
    }
  };

  myChart.setOption(option);

}

// 年度事故伤亡情况统计
function showLine1 (data) {
  var chart = echarts.init(document.getElementById('line1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      color:[
        '#214EFF',
        '#EB3432',
        '#7C899C',
        '#F2E52C',
      ],
      grid: {
        top: '38%',
        left: '3%',
        right: '4%',
        bottom: '5%',
        containLabel: true
      },
      legend: {
        right: '10',
        top: '45',
        itemWidth: 14,
        itemHeight: 8,
        data: ['事故起数', '死亡人数', '重伤人数','轻伤人数'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            }

          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '事故起数',
          type: 'line',
          data: data.data[0]
        },
        {
          name: '死亡人数',
          type: 'line',
          data: data.data[1]
        },
        {
          name: '重伤人数',
          type: 'line',
          data: data.data[2]
        },
        {
          name: '轻伤人数',
          type: 'line',
          data: data.data[3]
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

function showMap (mapdata, max, code) {
  var chart = echarts.init(document.getElementById('map'))
  $.get('./map/' + code + '.json', function (json) {

    echarts.registerMap(code, json)
    var option = {
      tooltip: {
        trigger: 'item'
      },
      visualMap: {
        min: 0,
        max: max,
        left: '20',
        top: 'bottom',
        textStyle: {
          color: '#4BCEEC'
        },
        text: ['高', '低'],           // 文本，默认为数值文本
        color: [
          '#284CA9',
          '#4DD5EF'],
        calculable: true
      },
      series: [
        {
          name: '企业自查自报隐患数',
          roam: true,
          type: 'map',
          zoom:1.2,
          mapType: code,
          label: {
            normal: {
              show: true,
              color: '#062E4C'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1.5,
            }
          },
          data: mapdata
        }
      ]
    }

    chart.setOption(option)
  })
  return chart
}

