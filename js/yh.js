//  下拉
$.divselect('#select-year', '#input-year')

var yearSelected,
  currYear = new Date().getFullYear()

$('#input-year').val(currYear)


//监听下拉
$('#select-year a').on('click', function (e) {
  yearSelected = $('#input-year').val()
})

  var gaugeData = {value: 22, name: '隐患整改率'};
  var pie1Data = [
    {value: 100, name: '一般隐患',code:'X2001'},
    {value: 180, name: '重大隐患',code:'X2002'}
  ]

  var bar1Data = {
    x: ['2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月'],
    series: [
      {
        name: '排查隐患数',
        data: [10, 52, 66, 334, 390, 330, 220]
      },
      {
        name: '已整改',
        data: [10, 52, 200, 334, 77, 330, 220]
      },
      {
        name: '超期未整改',
        data: [10, 66, 33, 10, 66, 3]
      },
      {
        name: '整改率',
        data: [10, 66, 33, 10, 66, 3]
      }
    ]

  }
  var bar2Data = {
    hy:
      {
        x: ['危险化学品 ','烟花爆竹','非煤矿山','冶金行业'],
        data: [1,1,1,1]
      },
    dq: {
      x: ['上城区 ','下城区','拱墅区','西湖区'],
      data: [
        [1,1,11,1],
        [1,1,22,1],
        [11,1,22,1],
        [11,111,12,1]
      ]
    }
  }



  var bar3Data = {
    y:['设备设施设备设施','设备设施','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市'],
    data:[ 11,10,9,8,7,6,5,4,3]
  }


  var mapData = [
    {
      'name': '漯河市',
      'value': 211,
      'code': '411100'
    },
    {
      'name': '洛阳市',
      'value': 1000,
      'code': '410300'
    }
  ]
  var mapmax = 1000

  var gauge = showGauge(gaugeData);
  var pie1 = showPie1(pie1Data)
  var bar1 = showBar1(bar1Data)
  var bar2 = showBar2(bar2Data)
  var bar3 = showBar3(bar3Data)
  var map = showMap(mapData, mapmax, '41')


  bar2.on('timelinechanged',function (curr) {
    var currIdx = curr.currentIndex;
    if (currIdx == 0){
      $('#bottom_right_title').html('各行业企业隐患分布情况').css('paddingLeft',33)
    }else if(currIdx == 1){
      $('#bottom_right_title').html('各月度隐患来源分布').css('paddingLeft',44)
    }
  })


  // 进入市级
  map.on('click', function (series) {
    var code = series.data.code
    var name = series.data.name
    if (code ==  '410181' || code == '410225' || code =='410482' || code =='410526' || code =='410728' || code =='411381' || code =='411481'
      || code =='411525' || code =='411628' || code =='411729' || code ==undefined){
      return
    }
    $('#currCity').text(name).show()
    showMap(undefined, mapmax, code)
  })

  //返回省级
  $('#province').on('click', function (e) {
    showMap(mapData, mapmax, '41')
    $('#currCity').hide()
  })






