//行政许可类型
function getColorByCodeOrName (code,name) {
  if (code == 'HE901' || name == '危险化学品类') {  // 危险化学品类
    return '#e93414'
  } else if (code == 'HE902' || name == '烟花爆竹类') {//烟花爆竹类
    return '#b91d1c'
  } else if (code == 'HE903' || name == '非煤矿山类') {//非煤矿山类
    return '#474646'
  } else if (code == 'HE904' || name == '建设项目审查') {//建设项目审查
    return '#7630ED'
  } else if (code == 'HLY01' || name == '中介机构资质') {//中介机构资质
    return '#21D376'
  } else {
    return '#018DD3'
  }
}

function getVisualMapColorByCodeOrName (code, name) {
  if (name == '危险化学品' || code == 'HE901') {  // 危险化学品
    return ['#E93414', '#FCDBD1']
  } else if (name == '烟花爆竹' || code == 'HE902') {//烟花爆竹
    return ['#B91D1C', '#F4D6D3']
  } else if (name == '非煤矿山' || code == 'HE903') {//非煤矿山
    return ['#474646', '#D2D2D2']
  } else if (name == '建设项目审查' || code == 'HE904') {//建设项目审查
    return ['#7630ED', '#E4D5FC']
  } else if (name == '中介机构资质' || code == 'HLY01') {//中介机构资质
    return ['#21D376', '#D2F6E3']
  } else {
    return ['#284CA9', '#4DD5EF']
  }
}

// 行政许可持证情况
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '行政许可持证情况',
          type: 'pie',
          radius: [35, 60],
          center: ['50%', '65%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color: function (value) {
                return getColorByCodeOrName(value.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart;
}
function showBar1 (data,pcode) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {

      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '10%',
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom: '8',
          startValue: 0,
          endValue: 4,
          // end: 70,
          height: '22',
          backgroundColor: 'rgba(9,75,127,0.2)',
          fillerColor: '#094B7F',
          borderColor: '#094B80',
          textStyle: {
            color: '#97B8C9'
          }
        }

      ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            },
            formatter: function (val) {
              return wrapByNum(val)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '行政许可持证情况',
          type: 'bar',
          barWidth: '50%',
          data: data.data,
          itemStyle:{
            normal:{
              color:function(){
                return getColorByCodeOrName(pcode)
              }
            }
          }
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

// 各类企业许可覆盖情况
function showBar2 (data) {
  var chart = echarts.init(document.getElementById('bar2'))
  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
          return params[0].name + ':</br>' +
            params[0].seriesName + '：' + params[0].data + '</br>' +
            params[1].seriesName + '：' + params[1].data+'</br>'+
            '覆盖率：'+toFixed2(params[0].data/(params[0].data +params[1].data ))
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '10%',
        bottom: '5%',
        containLabel: true
      },

      xAxis: [
        {
          type: 'value',

          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'category',
          inverse: true,
          data: data.y,
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false
          }
        }
      ],
      series: [
        {
          name: '持证数',
          stack: '1',
          type: 'bar',
          barWidth: '50%',
          yAxisIndex: 0,
          data: data.series,
          itemStyle: {
            normal: {
              color: function (value) {
                var colorList = [
                  '#E93414',
                  '#B91D1C',
                  '#474646',
                  '#27972E'
                ]
                return colorList[value.dataIndex]

              }
            }
          },
          label:{
            normal:{
              show:true,
              position:'insideRight',
              color:'#97B8C9',
            }
          }
        },
        {
          name: '未持证数',
          type: 'bar',
          stack: '1',
          barWidth: '50%',
          yAxisIndex: 0,
          data: data.series1,
          itemStyle: {
            normal: {
              color: '#21273C'
            }
          },
          label:{
            normal:{
              show:true,
              position:'insideRight',
              color:'#97B8C9'
            }
          }
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}
// 按时办结率
function showGauge (data) {
  var chart = echarts.init(document.getElementById('gauge1'))
  var option = {
    baseOption: {
      timeline: {
        axisType: 'category',
        orient: 'vertical',
        autoPlay: true,
        inverse: true,
        playInterval: 5000,
        top: 10,
        right:55,
        bottom: 10,
        width: 2,
        label: {
          normal: {
            padding:0,
            position:'left',
            textStyle: {
              color: '#0D80FE'
            }
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false,
        },
        data: data.timelineData,
        series: []
      }

    },
    options: []
  }

  for (var i = 0;i<data.data.length;i++){
    var serie = {
        series: {
          name: '按时办结率',
          type: 'gauge',
          radius: '80%',
          center: ["40%", "65%"], // 仪表位置
          data: data.data[i],
          detail: {
            formatter: function (value) {
              return value.toFixed(2) + '%';
            },

          },
          axisTick: {
            show: false,
            splitNumber: 1
          },
          axisLabel: {
            show: true,
            distance: 10,
            formatter: function (v) {
              switch (v + '') {
                case '10':
                  return '弱';
                case '30':
                  return '低';
                case '60':
                  return '中';
                case '90':
                  return '高';
                default:
                  return '';
              }
            },
          },
          //刻度线
          axisLine: {
            show: false,
            lineStyle: {
              width: 8,
              color: [
                [0.2, '#E51C23'],
                [0.4, '#F56923'],
                [0.8, '#F5A623'],
                [1, '#0D80FE']
              ]
            }
          },
          splitLine: {
            show: false,
            length: 2
          },
          pointer: {
            width: 2
          }
        }
    }
    option.options.push(serie);
  }
  chart.setOption(option)

  $(window).resize(function () {
      chart.resize()
    }
  )
}

// 按时办结率
function showBar3 (chartData) {
  var chart = echarts.init(document.getElementById('bar3'))
  var option = {
    baseOption: {
      timeline: {
        axisType: 'category',
        orient: 'vertical',
        autoPlay: true,
        inverse: true,
        playInterval: 5000,
        top: 10,
        right:55,
        bottom: 10,
        width: 2,
        label: {
          normal: {
            padding:0,
            position:'left',
            textStyle: {
              color: '#0D80FE'
            }
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false,
        },
        data: chartData.timelineData,
        series: []
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      dataZoom: [
        {
          show: true,
          bottom:'8',
          start: 0,
          end: 70,
          height:'22',
          backgroundColor:'rgba(9,75,127,0.2)',
          fillerColor:'#094B7F',
          borderColor:'#094B80',
          textStyle:{
            color:'#97B8C9'
          }
        }

      ],
      legend: {
        show: true,
        top: 40,
        right:80,
        itemWidth:10,
        itemHeight:10,
        itemGap:5,
        data: ['危险化学品类', '烟花爆竹类', '非煤矿山类','建设项目审查','中介机构资质'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      grid: {
        top: '35%',
        left: '6%',
        right: '13%',
        bottom: '15%',
        containLabel: true
      },
      xAxis: {
        type: 'category',
        axisLabel: {
          textStyle: {
            color: '#97B8C9'
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: '#07274D'
          }
        },
        axisTick: {
          show: true,
          lineStyle: {
            color: '#1178C9'
          }
        },

        data: chartData.x
      },
      yAxis: [
        {
          type: 'value',
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {

            show: true,

            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            show: true,
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            show: true,
            textStyle: {
              color: '#97B8C9'

            }

          }
        },
      ],
    },

    options: []
  }
  var timeSeriesData = chartData.data;
  var timeSeriesArr = [];
  for (var i = 0;i<timeSeriesData.length;i++){
    var seriesData = timeSeriesData[i];
    var seriesArr = [];
    for (var j = 0;j<seriesData.length;j++){
      seriesArr.push( {
        name: seriesData[j].name,
        type: 'bar',
        itemStyle: {
          normal: {
            color: getColorByCodeOrName(seriesData[j].code)
          }
        },
        data: seriesData[j].data

      })
    }
    timeSeriesArr.push({series:seriesArr});

  }
  option.options = timeSeriesArr;

  chart.setOption(option)
  $(window).resize(function () {
      chart.resize()
    }
  )
}

function showMap (mapdata, max, code,callback) {
  var chart = echarts.init(document.getElementById('map'))

  function getSerieTotal (data) {
    var total = 0
    for (var i = 0; i < data.length; i++) {
      total += data[i].value
    }
    return total
  }

  $.get('./map/' + code + '.json',function (json) {

    echarts.registerMap(code, json)
    var option = {
      tooltip: {
        trigger: 'item'
      },
      legend: {
        selectedMode: 'single',
        orient: 'vertical',
        itemWidth: 12,
        itemHeight: 12,
        x: '20',
        y: '70',
        icon: 'circle',
        data: [],
      },
      visualMap: {
        min: 0,
        max: max,
        right: '20',
        bottom: '18',
        textStyle: {
          color: '#4BCEEC'
        },
        text: ['高', '低'],           // 文本，默认为数值文本
        color: [
          '#284CA9',
          '#4DD5EF'],
        calculable: true
      },
      series: [
        
      ]
    }
    var firstLegendName = '';
    for (var i = 0; i < mapdata.length; i++) {
      var serie = {
        name: function () {
          var data = mapdata[i].data
          return mapdata[i].type + '(' + getSerieTotal(data) + ')'
        }(),
        selectedMode: 'false',//是否允许选中多个区域
        roam: true,
        type: 'map',
        zoom: 1.2,
        left: '200',
        map: code,
        label: {
          normal: {
            show: true,
            // color:getColorByCodeOrName(mapdata[i].code)
            color:'#1178c9'
          }
        },
        itemStyle: {
          normal: {
            borderColor: '#64B6FE',
            borderWidth: 1.5,
            color: getColorByCodeOrName(mapdata[i].code)
          }
        },
        data: mapdata[i].data
      }
      option.series.push(serie)
      if (i == 0) {
        option.legend.data.push({
          name: mapdata[i].type + '(' + getSerieTotal(option.series[0].data) + ')',
          textStyle: {
            color: '#018DD3'
          }
        })
      } else {
        option.legend.data.push({
          name: mapdata[i].type + '(' + getSerieTotal(option.series[i].data) + ')',
          textStyle: {
            color: getColorByCodeOrName(mapdata[i].code)
          }
        })
      }
      firstLegendName = mapdata[0].type + '(' + getSerieTotal(option.series[0].data) + ')';
      
    }
    chart.setOption(option)
    chart.dispatchAction({
      type: 'legendSelect',
      // 图例名称
      name: firstLegendName
  })
    if (callback){
      callback(chart);
    }else{
      return
    }


  })


}


