//  下拉
$.divselect('#select-year', '#input-year')
$('#radio-select input').on('click',function(e){
  if ($('#radio-year').attr('checked')){
    $('#select-year').show().siblings('.select-month').hide();
    // $.divselect('#select-year', '#input-year')
  }else if ($('#radio-month').attr('checked')){
    $('#select-month').show().siblings('.select-year').hide();
    $("#month-begin").simpleCanleder();

    $("#month-end").simpleCanleder();
  }
})
var yearSelected,
  currYear = new Date().getFullYear()

$('#input-year').val(currYear)
//监听下拉
$('#select-year a').on('click', function (e) {
  yearSelected = $('#input-year').val()
  getDataByYear(yearSelected)
})

getDataByYear(currYear)

var i=0,j=0;
var timer,timer1,timerInit,timerInit1;


function getDataByYear (yearSelected) {
  var pie1Data = [
    {value: 100, name: '中介机构资质', code: 'HLY01'},
    {value: 811, name: '危险化学品', code: 'HE901'},
    {value: 15, name: '烟花爆竹', code: 'HE902'},
    {value: 10, name: '非煤矿山', code: 'HE903'},
    {value: 110, name: '建设项目审查', code: 'HE904'}
  ]

  var bar1data = {
    x: [
      '危险化学品经营许可证危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
      '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
      '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）'
    ],
    data: [10, 52, 66, 334, 390, 330, 220]
  }

  var bar2data = {
    y: ['危险化学品企业', '烟花爆竹企业', '非煤矿山企业', '工贸八大行业企业'],
    series: [10, 52, 66, 334],
    series1: [55, 100, 22, 110]
  }

  var gaugeData = {
    timelineData: ['2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月',
      '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月'],
    data: [11.223, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]
  }

  var bar3Data = {
    timelineData: ['2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月',
      '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月', '2017年1月'],
    x: ['郑州市', '郑州市', '郑州市', '郑州市', '郑州市', '郑州市', '郑州市', '郑州市', '郑州市', '郑州市'],
    data: [
      [
        {name: '危险化学品类', code: 'HE901', data: [11, 2, 22, 1, 2, 3, 1,]},
        {name: '烟花爆竹类', code: 'HE902', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
        {name: '非煤矿山类', code: 'HE903', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
        {name: '建设项目审查', code: 'HE904', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
        {name: '中介机构资质', code: 'HLY01', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
      ],
      [
        {name: '危险化学品类', code: 'HE901', data: [22, 2, 3, 1, 2, 3, 1,]},
        {name: '烟花爆竹类', code: 'HE902', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
        {name: '非煤矿山类', code: 'HE903', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
        {name: '建设项目审查', code: 'HE904', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
        {name: '中介机构资质', code: 'HLY01', data: [11, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]},
      ],
    ]

  }
  var mapData = [
    {
      'type': '行政许可总数',
      'code': '',
      'data': [
        {
          'name': '洛阳市',
          'value': 22,
          'code': 410300
        },
        {
          'name': '漯河市',
          'value': 200,
          'code': 411100
        }
      ]
    },
    {
      'type': '非煤矿山',
      'code': 'HE903',
      'data': [
        {
          'name': '洛阳市',
          'value': 300,
          'code': '410300'
        },
        {
          'name': '漯河市',
          'value': 11,
          'code': '411100'
        }
      ]
    },
    {
      'type': '危险化学品',
      "code": 'HE901',
      'data': [
        {
          'name': '洛阳市',
          'value': 300,
          'code': '410300'
        },
        {
          'name': '漯河市',
          'value': 22,
          'code': '411100'
        }
      ]
    },
    // {
    //   'type': '烟花爆竹',
    //   'code': 'HE902',
    //   'data': [
    //     {
    //       'name': '洛阳市',
    //       'value': 33,
    //       'code': '410300'
    //     },
    //     {
    //       'name': '漯河市',
    //       'value': 200,
    //       'code': '411100'
    //     }
    //   ]
    // },
    // {
    //   'type': '中介机构资质',
    //   "code": 'HLY01',
    //   'data': [
    //     {
    //       'name': '洛阳市',
    //       'value': 300,
    //       'code': '410300'
    //     },
    //     {
    //       'name': '漯河市',
    //       'value': 11,
    //       'code': '411100'
    //     }
    //   ]
    // },
    // {
    //   'type': '建设项目审查',
    //   'code': 'HE904',
    //   'data': [
    //     {
    //       'name': '洛阳市',
    //       'value': 300,
    //       'code': '410300'
    //     },
    //     {
    //       'name': '漯河市',
    //       'value': 200,
    //       'code': '411100'
    //     }
    //   ]
    // }
  ];
  var mapData1 = [
    {
      'type': '行政许可总数',
      'code': '',
      'data': [
        {
          'name': '新安县',
          'value': 22,
        },
        {
          'name': '吉利区',
          'value': 200,
        },
        
      ]
    },
    {
      'type': '非煤矿山',
      'code': 'HE903',
      'data': [
        {
          'name': '新安县',
          'value': 300,
          'code': '410300'
        },
        {
          'name': '吉利区',
          'value': 11,
          'code': '411100'
        }
      ]
    },
    {
      'type': '危险化学品',
      "code": 'HE901',
      'data': [
        {
          'name': '新安县',
          'value': 300,
          'code': '410300'
        },
        {
          'name': '吉利区',
          'value': 11,
          'code': '411100'
        }
      ]
    },
  ];
  var mapmax = 1000

  var pie1 = showPie1(pie1Data)
  var bar1 = showBar1(bar1data, 'HE901')
  var bar2 = showBar2(bar2data)
  var gauge1 = showGauge(gaugeData)
  var bar3 = showBar3(bar3Data)
  pie1.on('click', function (param) {
    var pcode = param.data.code;
    var bar1data = {
      x: [
        '11111111（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
        '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
        '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）'
      ],
      data: [10, 52, 66, 334, 390, 330, 220]
    }
    showBar1(bar1data, pcode)
  })
  showMap(mapData, mapmax, '41', function (chart) {

    chart.on('legendselectchanged', function (e) {
      var selectedObj = e.selected;
      var selectName = e.name;
      var puerSelectName = e.name.slice(0, e.name.lastIndexOf('('));
      $.each(selectedObj, function (k, v) {
        if (selectedObj[selectName]) {
          var option = chart.getOption();
          option.visualMap = {
            color: getVisualMapColorByCodeOrName(undefined, puerSelectName)
          }
          chart.setOption(option)
        }
      })

    })
    // //进入市级
    chart.on('click', function (series) {
      var name = series.data!== undefined ? series.data.name :undefined
      var code = getCodeByName(name)
      if (code ==  '410100' || code == '410200' || code =='410300' || code =='410400' || code =='410500' || code =='410600' || code =='410700'
        || code =='410800' || code =='410900' || code =='411000' || code =='411200' ||code =='411300' || code =='411400' ||code =='411500' ||
        code =='411600' ||code =='411700' ||code =='411900'){
        $('#currCity').text(name).show()
        showMap(mapData1, mapmax, code)
      }else if(code == 'undefined'){
        return
      }

    })

    //返回省级
    $('#province').on('click', function (e) {
      showMap(mapData, mapmax, '41')
      $('#currCity').hide()
    })

  })
}

