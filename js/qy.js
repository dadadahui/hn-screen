//  下拉
$.divselect('#select-year', '#input-year')

var yearSelected,
  currYear = new Date().getFullYear()

$('#input-year').val(currYear)
//监听下拉
$('#select-year a').on('click', function (e) {
  yearSelected = $('#input-year').val()
  getDataByYear(yearSelected)
})

getDataByYear(currYear)

var i= 0;
var timer;




function getDataByYear (yearSelected) {
  var pie1Data = [
    {value: 100, name: '危险化学品',code:'SYS1601'},
    {value: 1, name: '烟花爆竹',code:'SYS1602'},
    {value: 15, name: '非煤矿山',code:'SYS1603'},
    {value: 10, name: '冶金行业',code:'SYS1604'},
    {value: 110, name: '有色金属',code:'SYS1605'},
    {value: 110, name: '建材行业',code:'SYS1606'},
    {value: 110, name: '机械制造',code:'SYS1607'},
    {value: 110, name: '轻工业',code:'SYS1608'},
    {value: 110, name: '纺织工业',code:'SYS1609'},
    {value: 110, name: '烟草行业',code:'SYS1610'},
    {value: 110, name: '商贸行业',code:'SYS1611'},
    {value: 110, name: '其他',code:'SYS1699'}
  ]

  var bar1data = {
    x: [
      '危险化学品经营许可证危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
      '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
      '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）'
    ],
    data: [10, 52, 66, 334, 390, 330, 220]
  }

  var bar2data = {
    x:['可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业','可燃爆粉尘作业'],
    data: [10, 52, 66, 334]
  }



  var bar3Data = {
    x:['郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市'],
    data:[ 11.223,2,3,1,2,3,1,2,3,1,2,3]
  }

  var bar4Data = {
    qygm:
      {
        x: ['微型 ','小型','中型','大型'],
        data: [1,1,1,1]
      },
    bzhdj: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,2,1,1]
    },
    cxdj: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,1,3,1]
    },
    fxdj: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,1,1,1]
    }
  }
  var mapData = [
    {
      'type':'生产经营单位总数',
      'code':'',
      'data':[
        {
          'name':'洛阳市',
          'value':22,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },
    {
      'type':'非煤矿山',
      'code':'SYS1603',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':11,
          'code': '411100'
        }
      ]
    },
    {
      'type':'危险化学品',
      "code":'SYS1601',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':22,
          'code': '411100'
        }
      ]
    },
    {
      'type':'烟花爆竹',
      'code':'SYS1602',
      'data':[
        {
          'name':'洛阳市',
          'value':33,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },
    {
      'type':'冶金行业',
      "code":'SYS1604',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':11,
          'code': '411100'
        }
      ]
    },
    {
      'type':'有色金属',
      'code':'SYS1605',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },
    {
      'type':'建材行业',
      'code':'SYS1606',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },
    {
      'type':'机械制造',
      'code':'SYS1607',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },{
      'type':'轻工业',
      'code':'SYS1608',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },{
      'type':'纺织工业',
      'code':'SYS1609',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    }
    ,{
      'type':'烟草行业',
      'code':'SYS1610',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    } ,{
      'type':'商贸行业',
      'code':'SYS1611',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    },{
      'type':'其他',
      'code':'SYS1699',
      'data':[
        {
          'name':'洛阳市',
          'value':300,
          'code': '410300'
        },
        {
          'name':'漯河市',
          'value':200,
          'code': '411100'
        }
      ]
    }
  ];
  var mapmax = 1000

  var mapData1 = [
    {
      'type': '生产经营单位总数',
      'code': '',
      data: [
        {
          'name': '新安县',
          'value': 22,
        },
        {
          'name': '吉利区',
          'value': 200,
        }
      ]
    },
    {
      'type': '危险化学品',
      "code": 'HLY01',
      'data': [
        {
          'name': '新安县',
          'value': 300,
          'code': '410300'
        },
        {
          'name': '吉利区',
          'value': 11,
          'code': '411100'
        }
      ]
    },
  ];

  var pie1 = showPie1(pie1Data)
  var bar1 = showBar1(bar1data,'SYS1601')
  var bar2 = showBar2(bar2data)
  var bar3 = showBar3(bar3Data)
  var bar4 = showBar4(bar4Data)

  pie1.on('click',function (param) {
    var pcode = param.data.code;
    console.log(pcode)
    var bar1data = {
      x: [
        '危险化学品经营许可证', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
        '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）',
        '危险化学品经营许可证（市级）', '危险化学品经营许可证（市级）'
      ],
      data: [10, 52, 66, 334, 390, 330, 220]
    }
    showBar1(bar1data,pcode)
  })
  var map = showMap(mapData, mapmax, '41',function(map){
    map.on('legendselectchanged',function (e) {
      var selectedObj = e.selected;
      var selectName = e.name;
      var puerSelectName = e.name.slice(0, e.name.lastIndexOf('('));
      $.each(selectedObj,function (k, v) {
        if (selectedObj[selectName]){
          var option = map.getOption();
          option.visualMap = {
            color: getVisualMapColorByCodeOrName(undefined,puerSelectName)
          }
          map.setOption(option)
        }
      })

    })

    //进入市级
    map.on('click', function (series) {
      var name = series.data!== undefined ? series.data.name :undefined
      var code = getCodeByName(name)
      if (code ==  '410100' || code == '410200' || code =='410300' || code =='410400' || code =='410500' || code =='410600' || code =='410700'
        || code =='410800' || code =='410900' || code =='411000' || code =='411200' ||code =='411300' || code =='411400' ||code =='411500' ||
        code =='411600' ||code =='411700' ||code =='411900'){
        $('#currCity').text(name).show()
        showMap(mapData1, mapmax, code)
      }else if(code == 'undefined'){
        return
      }

    })
  })




  //返回省级
  $('#province').on('click', function (e) {
    showMap(mapData, mapmax, '41')
    $('#currCity').hide()
  })

}
