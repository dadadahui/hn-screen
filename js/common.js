function toFixed2(num) {
  return Number(num*100).toFixed(2)+'%';
}

function wrapByNum(string) {
  return /\S{3}/.test(string)
    ? string.replace(/\s/g, '').replace(/(.{4})/g, "$1 \n")
    : string
}

function wrapByNum1(string) {
  return /\S{3}/.test(string)
    ? string.replace(/\s/g, '').replace(/(.{7})/g, "$1 \n")
    : string
}

function getCodeByName(name){
  switch (name){
    case '郑州市':
      return '410100'
    case '开封市':
      return '410200'
    case '洛阳市':
      return '410300'
    case '平顶山市':
      return '410400'
    case '安阳市':
      return '410500'
    case '鹤壁市':
      return '410600'
    case '新乡市':
      return '410700'
    case '焦作市':
      return '410800'
    case '濮阳市':
      return '410900'
    case '许昌市':
      return '411000'
    case '漯河市':
      return '411100'
    case '三门峡市':
      return '411200'
    case '南阳市':
      return '411300'
    case '商丘市':
      return '411400'
    case '信阳市':
      return '411500'
    case '周口市':
      return '411600'
    case '驻马店市':
      return '411700'
    case '济源市':
      return '411900'
  }
}