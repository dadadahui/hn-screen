var i= 0;
var timer;
var pie1Data = [
    {value: 100, name: '一级',code:'C8301'},
    {value: 1, name: '二级',code:'C8302'},
    {value: 15, name: '三级',code:'C8303'},
    {value: 15, name: '四级',code:'C8304'},
  ]

  var pie2Data = [
    {value: 100, name: '危化品生产'},
    {value: 1, name: '危化品经营'},
    {value: 15, name: '危化品使用'},
    {value: 15, name: '其他危化行业'},
  ]

  var bar1data = {
    x: [
      '危险化学品生产企业', '危险化学品经营（有储存设施）', '危险化学品经营（无储存设施）', '危险化学品使用企业','易制毒危化品',
      '油气输送管道'],
    data: [
      [10, 52, 66, 334, 390, 330],
      [10, 52, 66, 234, 390, 330]
    ]
  }
  var bar2data = {
    x: [
      '郑州市', '郑州市', '郑州市', '郑州市','郑州市',
      '郑州市', '郑州市', '郑州市', '郑州市','郑州市',
      '郑州市', '郑州市', '郑州市', '郑州市','郑州市',
    ],
    data: [
      [10, 52, 66, 334, 390, 330,10, 52, 66, 334, 390, 330],
      [10, 52, 66, 334, 390, 330,10, 52, 66, 334, 390, 330],
    ]
  }





  var bar3Data = {
    y:['氟代苯醚醛氟代苯醚醛','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市'],
    data:[ 10,9,8,7,6,5,4,3,2,1]
  }

  var mapData = [
  {
    'type':'重大危险源总数',
    'code':'',
    'data':[
      {
        'name':'洛阳市',
        'value':22,
        'code': '410300'
      },
      {
        'name':'漯河市',
        'value':200,
        'code': '411100'
      }
    ]
  },
  {
    'type':'一级',
    'code':'C8301',
    'data':[
      {
        'name':'洛阳市',
        'value':300,
        'code': '410300'
      },
      {
        'name':'漯河市',
        'value':11,
        'code': '411100'
      }
    ]
  },
  {
    'type':'二级',
    "code":'C8302',
    'data':[
      {
        'name':'洛阳市',
        'value':300,
        'code': '410300'
      },
      {
        'name':'漯河市',
        'value':22,
        'code': '411100'
      }
    ]
  },
  {
    'type':'三级',
    'code':'C8303',
    'data':[
      {
        'name':'洛阳市',
        'value':33,
        'code': '410300'
      },
      {
        'name':'漯河市',
        'value':200,
        'code': '411100'
      }
    ]
  },
  {
    'type':'四级',
    "code":'C8304',
    'data':[
      {
        'name':'洛阳市',
        'value':300,
        'code': '410300'
      },
      {
        'name':'漯河市',
        'value':11,
        'code': '411100'
      }
    ]
  }

];
  var mapmax = 1000
  var mapData1 = [
    {
      'type':'重大危险源总数',
      'code':'',
      data: [
        {
          'name': '新安县',
          'value': 22,
        },
        {
          'name': '吉利区',
          'value': 200,
        }
      ]
    }
  ]


  var pie1 = showPie1(pie1Data)
  var pie2 = showPie2(pie2Data)
  var bar1 = showBar1(bar1data)
  var bar2 = showBar2(bar2data)
  var bar3 = showBar3(bar3Data)

  var map = showMap(mapData, mapmax, '41',function(map){
    map.on('legendselectchanged',function (e) {
      var selectedObj = e.selected;
      var selectName = e.name;
      var puerSelectName = e.name.slice(0, e.name.lastIndexOf('('));
      $.each(selectedObj,function (k, v) {
        if (selectedObj[selectName]){
          var option = map.getOption();
          option.visualMap = {
            color: getVisualMapColorByCodeOrName(undefined,puerSelectName)
          }
          map.setOption(option)
        }
      })

    })
    // //进入市级
    map.on('click', function (series) {
      var name = series.data!== undefined ? series.data.name :undefined
      var code = getCodeByName(name)
      if (code ==  '410100' || code == '410200' || code =='410300' || code =='410400' || code =='410500' || code =='410600' || code =='410700'
        || code =='410800' || code =='410900' || code =='411000' || code =='411200' ||code =='411300' || code =='411400' ||code =='411500' ||
        code =='411600' ||code =='411700' ||code =='411900'){
        $('#currCity').text(name).show()
        showMap(mapData1, mapmax, code)
      }else if(code == 'undefined'){
        return
      }

    })
  })



  //返回省级
  $('#province').on('click', function (e) {
    showMap(mapData, mapmax, '41')
    $('#currCity').hide()
  })


