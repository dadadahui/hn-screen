//  下拉
$.divselect('#select-year', '#input-year')

var yearSelected,
  currYear = new Date().getFullYear()

$('#input-year').val(currYear)
//监听下拉
$('#select-year a').on('click', function (e) {
  yearSelected = $('#input-year').val()
  getDataByYear(yearSelected)
})

getDataByYear(currYear)

var i= 0;
var timer;




function getDataByYear (yearSelected) {
  var pie1Data = [
    {value: 100, name: '一级',code:'C8301'},
    {value: 1, name: '二级',code:'C8302'},
    {value: 15, name: '三级',code:'C8303'},
    {value: 15, name: '四级',code:'C8304'},
  ]

  var pie2Data = [
    {value: 100, name: '危化品生产'},
    {value: 1, name: '危化品经营'},
    {value: 15, name: '危化品使用'},
    {value: 15, name: '其他危化行业'},
  ]

  var bar1data = {
    y:['氟代苯醚醛氟代苯醚醛','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市','郑州市'],
    data:[10,9,8,7,6,5,4,3,2,1]
}
  var bar2data = {
    x: [
      '1月', '2月', '3月', '4月','5月','6月',
      '1月', '2月', '3月', '4月','5月','6月',
    ],
    data: [
      [10, 11, 66, 334, 390, 330],
      [10, 52, 66, 234, 20, 330],
      [10, 52, 22, 334, 390, 11],
      [11, 52, 66, 234, 390, 330],
    ]
  }





  var bar3Data = {
    x:[
      '安全设施缺少或有缺陷','个人防护用品缺少或有缺陷','生产场所环境不良存在事故隐患','安全设施缺少或有缺陷','安全设施缺少或有缺陷',
      '安全设施缺少或有缺陷','个人防护用品缺少或有缺陷','生产场所环境不良存在事故隐患','安全设施缺少或有缺陷','安全设施缺少或有缺陷',
    ],
    data:[10,9,8,7,6,5,4,3,2,1]
  }

  var line1Data = {
    x: [
      '1月', '2月', '3月', '4月','5月','6月',
      '1月', '2月', '3月', '4月','5月','6月',
    ],
    data: [
      [10, 11, 66, 334, 390, 330],
      [10, 52, 66, 234, 20, 330],
      [10, 52, 22, 334, 390, 11],
      [11, 52, 66, 234, 390, 330],
    ]
  }

  var bar4Data = {
    qygm:
      {
        x: ['微型 ','小型','中型','大型'],
        data: [1,1,1,1]
      },
    bzhdj: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,2,1,1]
    },
    cxdj: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,1,3,1]
    },
    fxdj: {
      x: ['一级 ','二级','三级','规范化企业'],
      data: [1,1,1,1]
    }
  }
  var mapData = [
    {
      'name': '漯河市',
      'value': 211,
      'code': '411100'
    },
    {
      'name': '洛阳市',
      'value': 1000,
      'code': '410300'
    }
  ]
  var mapmax = 1000

  //地图面板展示数据
  var boardData = {
    planCheck: 218,
    haschecked: 171,
    totalCheckTimes: 1111
  }

  function refreshBoard () {
    $('#planCheck').text(boardData.planCheck)
    $('#haschecked').text(boardData.haschecked)
    $('#rate').text(toFixed2(boardData.haschecked / boardData.planCheck))
    $('#totalCheckTimes').text(boardData.totalCheckTimes)
  }
  refreshBoard()

  // var pie1 = showPie1(pie1Data)
  // var pie2 = showPie2(pie2Data)
  var bar1 = showBar1(bar1data)
  var bar2 = showBar2(bar2data)
  var bar3 = showBar3(bar3Data)
  showBarRadius()
  var line1 = showLine1(line1Data)

  // var bar4 = showBar4(bar4Data)


  var map = showMap(mapData, mapmax, '41')
  map.on('legendselectchanged',function (e) {
    var selectedObj = e.selected;
    var selectName = e.name;
    var puerSelectName = e.name.slice(0, e.name.lastIndexOf('('));
    $.each(selectedObj,function (k, v) {
      if (selectedObj[selectName]){
        var option = map.getOption();
        option.visualMap = {
          color: getVisualMapColorByCodeOrName(undefined,puerSelectName)
        }
        map.setOption(option)
      }
    })

  })
  // console.log(map.getOption().legend)

  //进入市级
  map.on('click', function (series) {
    console.log(series)
    var code = series.data.code
    var name = series.data.name
    if (code ==  '410181' || code == '410225' || code =='410482' || code =='410526' || code =='410728' || code =='411381' || code =='411481'
      || code =='411525' || code =='411628' || code =='411729' || code ==undefined){
      return
    }
    $('#currCity').text(name).show()
    showMap(undefined, mapmax, code)
  })

  //返回省级
  $('#province').on('click', function (e) {
    showMap(mapData, mapmax, '41')
    $('#currCity').hide()
  })

}

