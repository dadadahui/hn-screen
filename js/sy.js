var pie1Data = [
  { value: 100, name: "一级", code: "C8301" },
  { value: 1, name: "二级", code: "C8302" },
  { value: 15, name: "三级", code: "C8303" },
  { value: 15, name: "四级", code: "C8304" }
];

var bar3Data = {
  x: [
    "安全设施缺少或有缺陷",
    "个人防护用品缺少或有缺陷",
    "生产场所环境不良存在事故隐患",
    "安全设施缺少或有缺陷",
    "安全设施缺少或有缺陷"
  ],
  data: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
};
var typeCode='SYS1601';
var mapData = [
  {
    name: "洛阳市",
    value: 532,
    code: "410300"
  },
  {
    name: "漯河市",
    value: 200,
    code: "411100"
  }
]
var mapData1 = [
  {
    name: "吉利区",
    value: 22,
    code: "1"
  },
  {
    name: "新安县",
    value: 200,
    code: "2"
  }
];
var mapmax = 1000;

var pie2Data = [
  { value: 100, name: "危险化学品", code: "SYS1601" },
  { value: 1, name: "烟花爆竹", code: "SYS1602" },
  { value: 15, name: "非煤矿山", code: "SYS1603" },
  { value: 10, name: "冶金行业", code: "SYS1604" },
  { value: 110, name: "有色金属", code: "SYS1605" },
  { value: 110, name: "建材行业", code: "SYS1606" },
  { value: 110, name: "机械制造", code: "SYS1607" },
  { value: 110, name: "轻工业", code: "SYS1608" },
  { value: 110, name: "纺织工业", code: "SYS1609" },
  { value: 110, name: "烟草行业", code: "SYS1610" },
  { value: 110, name: "商贸行业", code: "SYS1611" },
  { value: 110, name: "其他", code: "SYS1699" }
];
var pie2Data1 = [
  { value: 222, name: "危险化学品", code: "SYS1601" },
  { value: 1, name: "烟花爆竹", code: "SYS1602" },
  { value: 15, name: "非煤矿山", code: "SYS1603" },
  { value: 33, name: "冶金行业", code: "SYS1604" },
  { value: 110, name: "有色金属", code: "SYS1605" },
  { value: 222, name: "建材行业", code: "SYS1606" },
  { value: 110, name: "机械制造", code: "SYS1607" },
  { value: 110, name: "轻工业", code: "SYS1608" },
  { value: 110, name: "纺织工业", code: "SYS1609" },
  { value: 110, name: "烟草行业", code: "SYS1610" },
  { value: 110, name: "商贸行业", code: "SYS1611" },
  { value: 110, name: "其他", code: "SYS1699" }
];

var pie1 = showPie1(pie1Data);
var bar2 = showBar2();
var bar3 = showBar3(bar3Data);
var pie2 = showPie2(pie2Data);
showBarRadius();
showPieTimeLine();
showMap(mapData, mapmax, "41", function(map) {
  //进入市级
  map.on("click", function(series) {
    var name = series.data ? series.data.name : undefined;
    var code = series.data ? series.data.code : undefined;
    if (
      code == "410100" ||
      code == "410200" ||
      code == "410300" ||
      code == "410400" ||
      code == "410500" ||
      code == "410600" ||
      code == "410700" ||
      code == "410800" ||
      code == "410900" ||
      code == "411100" ||
      code == "411200" ||
      code == "411300" ||
      code == "411400" ||
      code == "411500" ||
      code == "411600" ||
      code == "411700" ||
      code == "411900"
    ) {
      $("#currCity")
        .text(name)
        .show();
      showMap(mapData1, mapmax, code);
    } else if (code == "undefined") {
      return;
    }
    showPie2(pie2Data1);
  });

  var timer1 = setInterval(function() {
    var code = startdispPieAction(pie2);
    var option = map.getOption();
    option.visualMap = {color: getVisualMapColorByCodeOrName(code)};
    map.setOption(option);
  }, 3000);
});

//返回省级
$("#province").on("click", function(e) {
  showMap(mapData, mapmax, "41");
  showPie2(pie2Data);

  $("#currCity").hide();
});

var currentIndex = -1;
function startdispPieAction(pie, cb) {
  var option = pie.getOption();
  var dataLen = option.series[0].data.length;
  // 取消之前高亮的图形
  pie.dispatchAction({
    type: "downplay",
    seriesIndex: 0,
    dataIndex: currentIndex
  });
  currentIndex = (currentIndex + 1) % dataLen;
  // 高亮当前图形
  pie.dispatchAction({
    type: "highlight",
    seriesIndex: 0,
    dataIndex: currentIndex
  });
  // 显示 tooltip
  pie.dispatchAction({
    type: "showTip",
    seriesIndex: 0,
    dataIndex: currentIndex
  });

  var code = option.series[0].data[currentIndex].code;
  return code;
}
