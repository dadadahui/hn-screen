//执法类型
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('zflx'))
  chart.setOption(
    {
      color: [
        '#0099FF',
        '#21D376',
        '#F5A623',
        '#EB3432',
        '#A4BCC6'
      ],
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '执法类型情况',
          type: 'pie',
          radius: [35, 60],
          center: ['50%', '60%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1'
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}
function showBarCheckRateRank (industry, checkRate) {
  var chart = echarts.init(document.getElementById('checkRateRank'))
  chart.setOption(
    {
      color: ['#21D376'],
      grid: {
        top: '10',
        bottom: '20',
        left: '35%',
        // containLabel: true
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      xAxis: [
        {
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#607D8B',
            formatter: '{value}%'
          },
          splitLine: {
            show: false
          },
          type: 'value',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'category',
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#607D8B',
            formatter: function (value) {
              if (value.length > 7) {
                return value.substr(0, 7) + '\n' + value.substr(7)
              } else {
                return value.substr(0, 7)
              }
            }
          },
          axisTick: {
            show: false
          },
          data: industry
        }
      ],
      series: [
        {
          type: 'bar',
          barWidth: '45%',
          label: {
            normal: {
              show: true,
              position: 'right',
              color: '#999',
              formatter: '{c}%'

            }
          },
          data: checkRate
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}

//各月执法情况
function showBar1 (data) {
  var chart = echarts.init(document.getElementById('perMonth'))

  chart.setOption(
    {
      color: [
        '#3157EC',
        '#00CC57',
        '#EB3432'
      ],
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['计划检查企业数', '已检查企业数', '完成率'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
          var res = ''
          for (var i = 0; i < params.length; i++) {
            var param = params[i]
            if (param.seriesName == '完成率') {
              var item = param.seriesName + ':' + param.data + '%'
            } else {
              var item = param.seriesName + ':' + param.data
            }
            res += '' + item + '</br>'
          }

          return res
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '10%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        },
        {
          type: 'value',
          max: 100,
          min: 0,
          axisLabel: {
            textStyle: {
              color: '#97B8C9'

            },
            formatter: function (v) {
              return v + '%'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false,
          }
        }
      ],
      series: [
        {
          name: '计划检查企业数',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[0].data
        },
        {
          name: '已检查企业数',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[1].data
        },
        {
          name: '完成率',
          type: 'line',
          yAxisIndex: 1,
          data: data.series[2].data
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

//各单位执法情况
function showBar2 (data) {
  var chart = echarts.init(document.getElementById('perUnit'))
  chart.setOption(
    {
      color: [
        '#1CA50E',
        '#FFAB49',
        '#fff'
      ],
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['计划检查企业数', '已检查企业数', '完成率'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
          var res = ''
          for (var i = 0; i < params.length; i++) {
            var param = params[i]
            if (param.seriesName == '完成率') {
              var item = param.seriesName + ':' + param.data + '%'
            } else {
              var item = param.seriesName + ':' + param.data
            }
            res += '' + item + '</br>'
          }
          return res
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom: '8',
          start: 0,
          end: 70,
          height: '22',
          backgroundColor: 'rgba(9,75,127,0.2)',
          fillerColor: '#094B7F',
          borderColor: '#094B80',
          textStyle: {
            color: '#97B8C9'
          }
        }

      ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            },
            formatter: function (value, index) {
              return value.slice(0, -3) + '\n' + value.substr(-3)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false
          }
        },
        {
          type: 'value',
          max: 100,
          min: 0,
          axisLabel: {
            textStyle: {
              color: '#97B8C9'

            },
            formatter: function (v) {
              return v + '%'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '计划检查企业数',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[0].data
        },
        {
          name: '已检查企业数',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[1].data
        },
        {
          name: '完成率',
          type: 'line',
          yAxisIndex: 1,
          data: data.series[2].data
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

//各类企业执法覆盖情况
function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'))
  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
          return params[0].name + ':</br>' +
            params[0].seriesName + '：' + params[0].data + '</br>' +
            params[1].seriesName + '：' + params[1].data+'</br>'+
            '覆盖率：'+toFixed2(params[0].data/(params[0].data +params[1].data ))
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '10%',
        bottom: '5%',
        containLabel: true
      },

      xAxis: [
        {
          type: 'value',

          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'category',
          inverse: true,
          data: data.y,
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false
          }
        }
      ],
      series: [
        {
          name: '被执法检查企业数',
          stack: '1',
          type: 'bar',
          barWidth: '50%',
          yAxisIndex: 0,
          data: data.series,
          itemStyle: {
            normal: {
              color: function (value) {
                var colorList = [
                  '#E93414',
                  '#B91D1C',
                  '#474646',
                  '#27972E'
                ]
                return colorList[value.dataIndex]

              }
            }
          },
          label:{
            normal:{
              show:true,
              position:'insideRight',
              color:'#97B8C9',
            }
          }
        },
        {
          name: '未被执法检查企业数',
          type: 'bar',
          stack: '1',
          barWidth: '50%',
          yAxisIndex: 0,
          data: data.series1,
          itemStyle: {
            normal: {
              color: '#21273C'
            }
          },
          label:{
            normal:{
              show:true,
              position:'insideRight',
              color:'#97B8C9'
            }
          }
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

//单位排名top5
function showTop5 (data) {

  var chart = echarts.init(document.getElementById('virtual'))
  var option = {
    baseOption: {
      animationDurationUpdate: 1000,
      animationEasingUpdate: 'quinticInOut',
      timeline: {
        axisType: 'category',
        left: '25',
        right: '25',
        autoPlay: true,
        playInterval: 10000,
        label: {
          normal: {
            textStyle: {
              color: '#0D80FE',
              padding: 10
            },
            position: 'bottom'
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false,
        },
        data: ['检查次数', '排查隐患数', '立案数', '处罚次数', '处罚金额']
      }

    },
    options: []
  }

  chart.setOption(option)
  chart.on('timelinechanged', function (curr) {
    var currIdx = curr.currentIndex
    changeGridData(currIdx)
  })
  function changeGridData (index) {
    var name = convertIdxToName(index)
    var dataArr = data[name]
    var htmlStr = ''
    for (var i = 0; i < dataArr.length; i++) {
      var dataItem = dataArr[i]
      htmlStr += '    <tr>' +
        '              <th>' + (i + 1) + '</th>' +
        '              <td>' + dataItem.unit + '</td>' +
        '              <td>' + dataItem.data + '</td>' +
        '            </tr>'
    }
    $('#grid-body').html(htmlStr)
  }

  function convertIdxToName (index) {
    switch (index) {
      case 0:
        return 'cfcs'
      case 1:
        return 'cfje'
      case 2:
        return 'jccs'
      case 3:
        return 'pcyhs'
      case 4:
        return 'las'
    }
  }

}

function showMap (mapdata, max, code,callback) {
  var chart = echarts.init(document.getElementById('map'))
  $.get('./map/' + code + '.json', function (json) {

    echarts.registerMap(code, json)
    var option = {
      tooltip: {
        trigger: 'item'
      },
      visualMap: {
        min: 0,
        max: max,
        left: '20',
        top: 'bottom',
        textStyle: {
          color: '#4BCEEC'
        },
        text: ['高', '低'],           // 文本，默认为数值文本
        color: [
          '#284CA9',
          '#4DD5EF'],
        calculable: true
      },
      series: [
        {
          name: '检查次数',
          roam: true,
          type: 'map',
          zoom:1.2,
          mapType: code,
          label: {
            normal: {
              show: true,
              color: '#062E4C'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1.5,
            }
          },
          data: mapdata
        }
      ]
    }

    chart.setOption(option)
    if(callback){
      callback(chart);
    }else{
      return;
    }
  })
}
