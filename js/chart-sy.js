//重大危险源等级
function getColorByCodeOrName(code,name) {
  if (code == "C8301" || name == '一级') {
    return "#D0021B";
  } else if (code == "C8302" || name == '二级') {
    return "#E02D00";
  } else if (code == "C8303" || name == '三级') {
    return "#E06000";
  } else if (code == "C8304" || name == '四级') {
    return "#E09900";
  }else{
    return '#018DD3'
  }
}

//行业类别
function getColorByCodeOrName1(code,name) {
  if (code == "SYS1601" || name == '危险化学品') {  // 危险化学品
    return "#FF4321";
  } else if (code == "SYS1602" || name == '烟花爆竹') {//烟花爆竹
    return "#B91D1C";
  } else if (code == "SYS1603" || name == '非煤矿山') {//非煤矿山
    return "#5C5F6B";
  } else if (code == "SYS1604" || name == '冶金行业') {//冶金行业
    return "#F99A1B";
  } else if (code == "SYS1605" || name == '有色金属') {//有色金属
    return "#7FA51D";
  } else if (code == "SYS1606" || name == '建材行业') {//建材行业
    return "#139AC2";
  } else if (code == "SYS1607" || name == '机械制造') {//机械制造
    return "#005D93";
  } else if (code == "SYS1608" || name == '轻工业') {//轻工业
    return "#48A325";
  } else if (code == "SYS1609" || name == '纺织工业') {//纺织工业
    return "#ED5A0D";
  } else if (code == "SYS1610" || name == '烟草行业') {//烟草行业
    return "#006C26";
  }  else if (code == "SYS1611" || name == '商贸行业') {//商贸行业
    return "#27962E";
  } else if(code == "SYS1699" || name == '其他') {//其他
    return "#5C5F6B";
  }else{
    return '#018DD3';
  }
}
function getVisualMapColorByCodeOrName (code, name) {
  if (code == "SYS1601" || name == '危险化学品') {  // 危险化学品
    return ["#E93414","#FCDBD1"];
  } else if (code == "SYS1602" || name == '烟花爆竹') {//烟花爆竹
    return ["#B91D1C","#F4D6D3"];
  } else if (code == "SYS1603" || name == '非煤矿山') {//非煤矿山
    return ["#474646","#D2D2D2"];
  } else if (code == "SYS1604" || name == '冶金行业') {//冶金行业
    return ["#F99A1B","#FEEED3"];

  } else if (code == "SYS1605" || name == '有色金属') {//有色金属
    return ["#7FA51D","#E9EFD3"];

  } else if (code == "SYS1606" || name == '建材行业') {//建材行业
    return ["#139AC2","#CCEEF5"];

  } else if (code == "SYS1607" || name == '机械制造') {//机械制造
    return ["#005D93","#CCE3ED"];

  } else if (code == "SYS1608" || name == '轻工业') {//轻工业
    return ["#48A325","#DDEFD6"];

  } else if (code == "SYS1609" || name == '纺织工业') {//纺织工业
    return ["#ED5A0D","#FDE2CE"];
  } else if (code == "SYS1610" || name == '烟草行业') {//烟草行业
    return ["#006C26","#D5EDD8"];
  }  else if (code == "SYS1611" || name == '商贸行业') {//商贸行业
    return ["#27972E","#D5EDD8"];
  } else if(code == "SYS1699" || name == '其他') {//其他
    return ["#5C5F6B","#E2E3E5"];
  } else{
    return  [ '#284CA9', '#4DD5EF']
  }
}
//重大危险源等级占比情况
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '重大危险源等级占比情况',
          type: 'pie',
          radius: [40, 60],
          center: ['50%', '60%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine:{
            normal:{
              length:2,
              length1:2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color:function(val){
                return getColorByCodeOrName(val.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}

// 事故情况分析
function showBarRadius() {
  var myChart = echarts.init(document.getElementById('bar-radius'));
  var option = {
    tooltip: {
      trigger: 'item'
    },

    angleAxis: {
      axisLabel: {
        color: '#97B8C9',
        margin: 2
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.2)'
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: ['rgba(16,129,208,.2)']
        }
      },
      axisTick: {
        show: false
      }
    },
    radiusAxis: {
      show: false,
      // nameLocation: 'start',
      // nameGap: 2,
      type: 'category',
      splitArea: {
        show: true,
        areaStyle: {
          color: 'rgba(16,129,208,.2)'
        }
      },

      data: [
        '危险化学品',
        '烟花爆竹',
        '非煤矿山',
        '冶金行业',
        '有色金属',
        '建材行业',
        '机械制造',
        '轻工业',
        '纺织工业',
        '烟草行业',
        '商贸行业',
        '其他'
      ],
      z: 30
    },
    color: [
      '#BBF3FF',
      '#1178C9',
      '#4CAF50',
      '#8B572A',
      '#F8E71C',
      '#73BCF0',
      '#DC1F7A',
      '#EB3432',
      '#F5A623',
      '#7630ED',
      '#3157EC',
      '#6ECFE6',
      '#971F2E',
      '#9A911A',
      '#7ED321',
      '#5A3D23',
      '#563491',
      '#91949C',
      '#28335F',
      '#B09A75'
    ],
    polar: {
      radius:'60%',
      center: ['63%', '55%']
    },
    series: [
      {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        name: '物体打击',
        stack: 'a',
        radius: [
          '0%', '10%'
        ],
        center: [
          '63%', '55%'
        ],

      },
      {
        type: 'bar',
        data: [
          0,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        name: '机械伤害',
        center: [
          '63%', '55%'
        ],
        stack: 'a'
      },
      {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        name: '起重伤害',

        center: [
          '63%', '55%'
        ],
        stack: 'a'
      }, {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12,
          13
        ],
        coordinateSystem: 'polar',
        center: [
          '63%', '55%'
        ],

        name: '淹溺',
        stack: 'a'
      }, {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        center: [
          '63%', '55%'
        ],

        name: '灼烫',
        stack: 'a'
      },
      {
        type: 'bar',
        data: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12
        ],
        coordinateSystem: 'polar',
        center: [
          '63%', '55%'
        ],

        name: '火灾',
        stack: 'a'
      },
    ],
    legend: {
      show: true,
      type: 'scroll',
      orient: 'vertical',
      top:'50px',
      left: '26px',
      pageIconColor: '#fff',
      pageIconInactiveColor: '#ACCAD0',
      pageTextStyle: {
        color: '#ACCAD0'
      },
      itemWidth: 14,
      itemHeight: 8,
      textStyle: {
        color: '#97B8C9',
        fontSize: 12
      },
      data: [
        '物体打击',
        '车体伤害',
        '机械伤害',
        '起重伤害',
        '触电',
        '淹溺',
        '灼烫',
        '火灾',
        '高处坠落',
        '坍塌',
        '冒顶片帮',
        '透水',
        '放炮',
        '火药爆炸',
        '瓦斯爆炸',
        '锅炉爆炸',
        '容器爆炸',
        '其他爆炸',
        '中毒和窒息',
        '其他伤害'
      ]
    }
  };

  myChart.setOption(option);

}
// 职业病危害因素TOP5
function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '5%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            },
            formatter:function(val){
              return wrapByNum(val)
            }

          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          }
        }
      ],
      series: [
        {
          name: '',
          type: 'bar',
          barWidth:'55%',
          itemStyle:{
            normal:{
              color:'#21D376'
            }
          },
          data: data.data
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}
// 标准化等级分布情况
function showPieTimeLine() {

  var myChart = echarts.init(document.getElementById('pie-timeLine'));
  var option = {
    baseOption: {
      tooltip: {
        // trigger: 'axis'
      },
      legend: {
        itemWidth: 14,
        top: '8%',
        x: '33%',
        itemHeight: 8,
        textStyle: {
          color: '#97B8C9'
        },
        itemGap: 8,
        data: [

          '一级',
          '二级',
          '三级',
          '未定级',
          '标准化',
          // '诚信'
        ]
      },
      timeline: {
        axisType: 'category',
        orient: 'vertical',
        autoPlay: true,
        inverse: true,
        playInterval: 5000,
        symbol: 'image://./images/time-line-icon.png',
        symbolSize: 10,
        left: null,
        right: 5,
        top: 58,
        bottom: 16,
        width: 46,
        height: null,
        label: {
          normal: {
            textStyle: {
              color: '#4BCEEC'
            }
          },
          emphasis: {
            textStyle: {
              color: '#97B8C9'
            }
          }
        },

        lineStyle: {
          width: 1,
          color: '#4BCEEC',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },

        controlStyle: {
          show: false,
          showNextBtn: false,
          showPrevBtn: false,
          normal: {
            color: '#666',
            borderColor: '#666'
          },
          emphasis: {
            color: '#aaa',
            borderColor: '#aaa'
          }
        },
        itemStyle: {},
        data: ['2017', '2016']
      },
      grid: {
        left: '40%',
        right: '13%',
        bottom: '18%',
        // containLabel: true
      },
      yAxis: [
        {
          type: 'value',
          // interval: 0,
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            show: false,

            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            show: true,
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            show: true,
            textStyle: {
              color: '#97B8C9'

            },
            formatter: '{value} '
          }
        }

      ],
      xAxis: [
        {
          type: 'category',
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            },
            formatter: function(params) {
              if (params.length > 7) {
                return params.substring(0, 7) + '\n' + params.substring(7, params.length)
              }
              return params
            }

          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          data: [
            '一级', '二级', '三级', '未定级'
          ],
        }

      ],
      series: [
        {
          name: '标准化',
          type: 'bar',
          itemStyle: {
            normal: {
              color: '#3157EC'
            }
          }
        },
        {
          name: '标准化',
          type: 'pie',
          center:['30%','50%'],
          itemStyle: {
            normal: {
            }
          }
        },
      ],
      color: ['#3157EC', '#00A77B', '#F1951F', '#9C9C9C']
    },
    options: [
      { // 这是'2017/02' 对应的 option

        series: [
          {
            type: 'bar',
            data: [10, 52, 334, 220]
          },
          {
            type: 'pie',
            center:['18%','60%'],
            radius:[0,'50%'],
            data: [
              {
                name: '一级',
                value: 123
              }, {
                name: '二级',
                value: 213
              }, {
                name: '三级',
                value: 213
              }, {
                name: '未定级',
                value: 213
              }

            ]
          }
        ]
      }, 
      { // 这是'2017/02' 对应的 option

        series: [
          {
            type: 'bar',
            data: [1011, 52, 334, 220]
          }, 
          {
            type: 'pie',
            data: [
              {
                name: '一级',
                value: 143
              }, {
                name: '二级',
                value: 213
              }, {
                name: '三级',
                value: 213
              }, {
                name: '未定级',
                value: 213
              }

            ]
          }
        ]
      }, 
     
      
    ]
  };
  myChart.setOption(option);

}
// 各行业隐患排查整改情况
function showBar2() {
  var myChart = echarts.init(document.getElementById('bar2'));
  var option = {

    legend: {
      show: true,
      top: '8%',
      left: '37%',
      data: [
        '隐患排查数', '整改数', '整改率'

      ],
      itemWidth: 14,
      itemHeight: 8,
      textStyle: {
        color: '#97B8C9',
        fontSize: 12
      }
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {            // 坐标轴指示器，坐标轴触发有效
        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
      },
      formatter: function(params) {
        var str = '';

        $.each(params, function(index, el) {

          if (!index) {
            str += el.name + '<br/>' + '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + el.color + '"></span>' + el.seriesName + ' : ' + el.data + '<br/>';

          } else if (index === params.length - 1) {
            str += '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + el.color + '"></span>' + el.seriesName + ' : ' + el.data + '%<br/>'

          } else {
            str += '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + el.color + '"></span>' + el.seriesName + ' : ' + el.data + '<br/>'
          }
        });
        return str
      }

    },
    grid: {
      top: '26%',
      left: '3%',
      right: '3%',
      bottom: '5%',
      containLabel: true
    },

    xAxis: {
      type: 'category',
      axisLabel: {
        interval: 0,
        formatter: function(params) {

            if (params.length > 2) {
                var _name = params.substring(0, 2) + '\n' + params.substring(2, params.length);
                return _name;
            }
            return params
        },
        textStyle: {
          color: '#97B8C9'
        }
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#07274D'
        }
      },
      axisTick: {
        show: true,
        lineStyle: {
          color: '#1178C9'
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: '#07274D'
        }
      },
      data: [
        '危险化学品',
        '烟花爆竹',
        '非煤矿山',
        '冶金行业',
        '有色金属',
        '建材行业',
        '机械制造',
        '轻工业',
        '纺织工业',
        '烟草行业',
        '商贸行业',
        '其他'

      ]
    },
    yAxis: [
      {
        type: 'value',
        name: '',
        splitNumber: 5,
        splitLine: {
          show: true,
          lineStyle: {
            color: '#07274D'
          }
        },
        axisLine: {

          show: true,

          lineStyle: {
            color: '#07274D'
          }
        },
        axisTick: {
          show: true,
          lineStyle: {
            color: '#1178C9'
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#97B8C9'

          }

        }
      }, {
        type: 'value',
        name: '',
        min: 0,
        max: 100,
        splitNumber: 5,
        splitLine: {
          show: false,
          lineStyle: {
            color: '#07274D'
          }
        },
        axisLine: {

          show: false,

          lineStyle: {
            color: '#07274D'
          }
        },
        axisTick: {
          show: true,
          lineStyle: {
            color: '#1178C9'
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#97B8C9'

          },
          formatter: '{value}%'
        }
      }
    ],
    series: [
      {
        name: '隐患排查数',
        type: 'bar',

        itemStyle: {
          normal: {
            color: '#7630ED'
          }
        },
        data: [
          1,
          22,
          2,

          3,
          22,
          2,
          5,
          23,
          3,
          12,
          32,
          123
        ]

      },

      {
        name: '整改数',
        type: 'bar',
        itemStyle: {
          normal: {
            color: '#FF7E22'
          }
        },
        data: [
          11,
          122,
          12,
          321,
          23,
          13,
          122,
          12,
          12,
          1,
          43,
          23
        ]

      }, {             name: '整改率',
        type: 'line',
        yAxisIndex: 1,
        itemStyle: {
          normal: {
            color: '#21D376'
          }
        },
        data: [
          1,
          22,
          2,
          5,
          3,
          22,
          2,
          32,
          57,
          23,
          23,
          45
        ]

      }
    ]
  };

  myChart.setOption(option);

}


// 企业情况
function showMap(mapdata,mapmax,code,callback) {
  var myChart = echarts.init(document.getElementById('map'));
  $.getJSON('./map/'+code+'.json', function(zhejiang) {
    echarts.registerMap('zhejiang', zhejiang);
    var option = {
      tooltip: {},

      visualMap: {
        show: true,
        min:0,
        max: mapmax,
        seriesIndex: 0,
        left: '10',
        top: 'bottom',
        text: [
          '高', '低'
        ],
        textStyle: {
          color: '#4BCEEC'
        },
        calculable: true,
        color: ["#E93414","#FCDBD1"]
      },

      series: [
        {
          name: '企业分布情况',
          type: 'map',
          left: '2%',
          bottom: '2%',
          top: '6%',
          roam: true,
          y: 'center',
          zoom:0.8,
          mapType: 'zhejiang',
          label: {
            normal: {
              show: true,
              textStyle: {
                color: '#062E4C'
              }
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1.5,
            }
          },
          data: mapdata
        }

      ]

    }
    myChart.setOption(option);
    if(callback){
      callback(myChart);
    }else{
      return;
    }

  });
  
}

// 企业情况
function showPie2 (data) {
  var chart = echarts.init(document.getElementById('pie2'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      color: [
        '#E93414',
        '#B91D1C',
        '#1E1D25',
        '#F99A1B',
        '#7FA51D',
        '#139AC2',
        '#005D93',
        '#48A325',
        '#ED5A0D',
        '#006C26',
        '#27972E',
        '#535F8D'
      ],
      series: [
        {
          name: '企业分布情况',
          type: 'pie',
          radius: [
            '15%', '38%'
          ],
          center: [
            '50%', '50%'
          ],
          tooltip: {
            trigger: 'item',
            formatter: "{a}<br/>{b}: {c} ({d}%)"
          },
          label: {
            normal: {
              formatter: '{b} \n {d}%',
              show: true,
              textStyle: {
                fontSize: '12',
                color: '#97B8C9'
              }

            },
            emphasis: {
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'bold'
              }
            }
          },
          itemStyle: {
              borderColor: '#fff',
              borderWidth: 1.5,
              color:function(val){
                getColorByCodeOrName1(val.data.code)
              }
          },
          data: data
        }
      ]
    }
  )
  return chart;
}
