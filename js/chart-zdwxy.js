//重大危险源等级
function getColorByCodeOrName(code,name) {
  if (code == "C8301" || name == '一级') {
    return "#D0021B";
  } else if (code == "C8302" || name == '二级') {
    return "#E02D00";
  } else if (code == "C8303" || name == '三级') {
    return "#E06000";
  } else if (code == "C8304" || name == '四级') {
    return "#E09900";
  }else{
    return '#018DD3'
  }
}

function getVisualMapColorByCodeOrName (code, name) {
  if (code == "C8301" || name == '一级') {
    return ["#D0021B","#FFDBDF"];
  } else if (code == "C8302" || name == '二级') {
    return ["#E02D00","#FFE2DB"];
  } else if (code == "SYS1603" || name == '三级') {
    return ["#E06000","#FFEADB"];
  } else if (code == "C8304" || name == '四级') {
    return ["#E09900","#FFF3D9"];
  } else{
    return ['#284CA9','#4DD5EF'];
  }
}
//重大危险源等级占比情况
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '重大危险源等级占比情况',
          type: 'pie',
          radius: [40, 60],
          center: ['50%', '60%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine:{
            normal:{
              length:2,
              length1:2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color:function(val){
                return getColorByCodeOrName(val.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}
//重大危险源企业行业分布占比情况
function showPie2 (data) {
  var chart = echarts.init(document.getElementById('pie2'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      color:['#D0021B','#E02D00','#E06000','#AF260E'],
      series: [
        {
          name: '重大危险源等级占比情况',
          type: 'pie',
          radius: [40, 60],
          center: ['50%', '60%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine:{
            normal:{
              length:2,
              length1:2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}
// 重大危险源行业分布
function showBar1 (data) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {
      color: [
        '#0063C7',
        '#21D376'
      ],
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['重大危险源企业数', '重大危险源数量'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '2%',
        containLabel: true
      },
      // dataZoom: [
      //   {
      //     show: true,
      //     bottom: '8',
      //     startValue: 0,
      //     endValue: 4,
      //     // end: 70,
      //     height: '22',
      //     backgroundColor: 'rgba(9,75,127,0.2)',
      //     fillerColor: '#094B7F',
      //     borderColor: '#094B80',
      //     textStyle: {
      //       color: '#97B8C9'
      //     }
      //   }
      //
      // ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            },
            formatter: function (val) {
              return wrapByNum1(val)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '重大危险源企业数',
          type: 'bar',
          data: data.data[0]
        },
        {
          name: '重大危险源数量',
          type: 'bar',
          data: data.data[1]
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}
// 重大危险源地区分布
function showBar2 (data) {
  var chart = echarts.init(document.getElementById('bar2'))

  chart.setOption(
    {
      color: [
        '#0063C7',
        '#21D376'
      ],
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['重大危险源企业数', '重大危险源数量'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom: '8',
          startValue: 0,
          endValue: 4,
          // end: 70,
          height: '22',
          backgroundColor: 'rgba(9,75,127,0.2)',
          fillerColor: '#094B7F',
          borderColor: '#094B80',
          textStyle: {
            color: '#97B8C9'
          }
        }

      ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            },
            formatter: function (val) {
              return wrapByNum1(val)
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      series: [
        {
          name: '重大危险源企业数',
          type: 'bar',
          data: data.data[0]
        },
        {
          name: '重大危险源数量',
          type: 'bar',
          data: data.data[1]
        }

      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

//危险物质top10
function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'))
  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      color:['#29A1F7'],
      grid: {
        top: '15%',
        left: '3%',
        right: '10%',
        bottom: '5%',
        containLabel: true
      },

      xAxis: [
        {
          type: 'value',

          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'category',
          inverse: true,
          data: data.y,
          axisLabel : {
            textStyle: {
              color: '#97B8C9'
            },
            interval:0,
            formatter:function (val) {
              if  (val.length >= 6){
                return val.substring(0,6) + '...'
              }
              return val;
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false
          }
        }
      ],
      series: [
        {
          name: '在线量t',
          type: 'bar',
          barWidth: '50%',
          data: data.data,
          label:{
            normal:{
              show:true,
              position:'right',
              color:'#97B8C9'
            }
          }
        }


      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

function showMap (mapdata, max, code,callback) {
  var chart = echarts.init(document.getElementById('map'))

  function getSerieTotal (data) {
    var total = 0
    for (var i = 0; i < data.length; i++) {
      total += data[i].value
    }
    return total
  }

  $.get('./map/' + code + '.json', {async:false},function (json) {

    echarts.registerMap(code, json)
    var option = {
      tooltip: {
        trigger: 'item'
      },
      legend: {
        selectedMode: 'single',
        orient: 'vertical',
        itemWidth: 12,
        itemHeight: 12,
        x: '20',
        y: '70',
        icon: 'circle',
        data: [],
      },
      visualMap: {
        min: 0,
        max: max,
        right: '20',
        bottom: '18',
        textStyle: {
          color: '#4BCEEC'
        },
        text: ['高', '低'],           // 文本，默认为数值文本
        color: [
          '#284CA9',
          '#4DD5EF'],
        calculable: true
      },
      series: [
        {
          name: '生产经营单位总数',
          roam: true,
          type: 'map',
          zoom: 1.2,
          mapType: code,
          label: {
            normal: {
              show: true,
              color: '#062E4C'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1.5
            }
          },
          data: mapdata
        }
      ]
    }
    for (var i = 0; i < mapdata.length; i++) {
      var serie = {
        name: function () {
          var data = mapdata[i].data
          return mapdata[i].type + '(' + getSerieTotal(data) + ')'
        }(),
        roam: true,
        type: 'map',
        zoom: 1.2,
        left: '200',
        map: code,
        label: {
          normal: {
            show: true,
            color: '#006ACC'
          }
        },
        itemStyle: {
          normal: {
            borderColor: '#64B6FE',
            borderWidth: 1.5,
            areaColor: '#D1F1FF',
            shadowOffsetX: 4,
            shadowOffsetY: 4,
            shadowColor: '#50708d',
            shadowBlur: 20,
            color: getColorByCodeOrName(mapdata[i].code)
          }
        },
        data: mapdata[i].data
      }
      option.series.push(serie)
      if (i == 0) {
        option.legend.data.push({
          name: mapdata[i].type + '(' + getSerieTotal(option.series[1].data) + ')',
          textStyle: {
            color: '#018DD3'
          }
        })
      } else {
        option.legend.data.push({
          name: mapdata[i].type + '(' + getSerieTotal(option.series[i + 1].data) + ')',
          textStyle: {
            color: getColorByCodeOrName(mapdata[i].code)
          }
        })
      }
    }

    chart.setOption(option)

    if (callback){
      callback(chart);
    }else{
      return
    }
  })
  return chart
}
