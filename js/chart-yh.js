function getColorByCodeOrName (code,name) {
  if (code == 'X2001' || name == '一般隐患') {  // 一般隐患
    return '#F5A623'
  } else if (code == 'X2002' || name == '重大隐患') {//重大隐患
    return '#EB3432'
  }
}

//隐患整改率
function showGauge(data) {
  var chart = echarts.init(document.getElementById('gauge'));
  chart.setOption(
    {
      series : [
        {
          name:'',
          type:'gauge',
          radius:'100%',
          center:['50%','50%'],
          detail : {
            formatter:'{value}%',
            fontSize:'12'
          },
          data:data,
          axisTick:{
            length:0
          },
          axisLabel: {
            show: true,
            distance: -20,
          },
          //刻度线
          axisLine: {
            show: false,
            distance: 10,
            lineStyle: {
              width: 8,
              color: [
                [0.2, '#E51C23'],
                [0.8, '#F5A623'],
                [1, '#0D80FE']
              ]
            }
          },
          splitLine:{
            show:false
          },
          pointer:{
            width:2
          },
          title:{
            color:'#63869E',
            fontSize:12
          }
        }
      ]
    }
  )
  $(window).resize(function(){
      chart.resize();
    }
  )
}

// 隐患等级情况统计
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name: '隐患等级情况统计',
          type: 'pie',
          radius: [30, 55],
          center: ['50%', '55%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine:{
            normal:{
              length:2,
              length1:2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color:function(val){
                return getColorByCodeOrName(val.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}

//各月隐患治理情况统计
function showBar1 (data) {
  var chart = echarts.init(document.getElementById('bar1'))

  chart.setOption(
    {
      color: [
        '#7630ED',
        '#FF7E22',
        '#B91D1C',
        '#21D376'
      ],
      legend: {
        right: '100',
        top: '10',
        itemWidth: 14,
        itemHeight: 8,
        data: ['排查隐患数', '已整改', '超期未整改','整改率'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
          var res = ''
          for (var i = 0; i < params.length; i++) {
            var param = params[i]
            if (param.seriesName == '整改率') {
              var item = param.seriesName + ':' + param.data + '%'
            } else {
              var item = param.seriesName + ':' + param.data
            }
            res += '' + item + '</br>'
          }

          return res
        }
      },
      grid: {
        top: '25%',
        left: '3%',
        right: '4%',
        bottom: '15%',
        containLabel: true
      },
      dataZoom: [
        {
          show: true,
          bottom:'8',
          start: 0,
          end: 70,
          height:'22',
          backgroundColor:'rgba(9,75,127,0.2)',
          fillerColor:'#094B7F',
          borderColor:'#094B80',
          textStyle:{
            color:'#97B8C9'
          }
        }

      ],
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        },
        {
          type: 'value',
          max: 100,
          min: 0,
          axisLabel: {
            textStyle: {
              color: '#97B8C9'

            },
            formatter: function (v) {
              return v + '%'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false,
          }
        }
      ],
      series: [
        {
          name: '排查隐患数',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[0].data
        },
        {
          name: '已整改',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[1].data
        },
        {
          name: '超期未整改',
          type: 'bar',
          yAxisIndex: 0,
          data: data.series[2].data
        },
        {
          name: '整改率',
          type: 'line',
          yAxisIndex: 1,
          data: data.series[3].data
        }
      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

//各行业企业隐患分布情况 切换
function showBar2 (data) {

  var chart = echarts.init(document.getElementById('bar2'))
  var option = {
    baseOption: {
      animationDurationUpdate: 1000,
      animationEasingUpdate: 'quinticInOut',
      timeline: {
        axisType: 'category',
        left: '300',
        right: '200',
        top:'10',
        autoPlay: true,
        playInterval: 30000,
        label: {
          normal: {
            textStyle: {
              color: '#0D80FE',
              padding: 10
            },
            position: 'bottom'
          },
          emphasis: {
            textStyle: {
              color: '#4BCEEC'
            }
          }
        },
        symbol: 'image://./images/time-line-icon.png',

        lineStyle: {
          width: 1,
          color: '#0D80FE',
          type: 'dotted'
        },
        checkpointStyle: {
          symbol: 'image://./images/checkpoint-style-icon.png'
        },
        controlStyle: {
          show: false
        },
        data: ['各行业企业隐患分布情况', '各地区隐患来源分布']
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
      },
      xAxis: [
        {
          type: 'category',
          data: data.x,
          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'
            }
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLabel: {
            textStyle: {
              color: '#97B8C9'
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
        }
      ],
      dataZoom: [
        {
          show: true,
          bottom:'8',
          start: 0,
          end: 70,
          height:'22',
          backgroundColor:'rgba(9,75,127,0.2)',
          fillerColor:'#094B7F',
          borderColor:'#094B80',
          textStyle:{
            color:'#97B8C9'
          }
        }

      ],
      grid:{
        top:'40%',
        bottom:'25%',
        left:'8%',
        right:'8%'
      },
      legend: {
        right: '150',
        top: '50',
        itemWidth: 14,
        itemHeight: 8,
        textStyle: {
          color: '#6F7B8D'
        }
      },
    },
    options: [{
      legend: {
        show:false,
        data:[]
      },
      series:[
        {
          type: 'bar',
          name:'',
          barWidth: '50%',
          itemStyle: {
            normal: {
              color: '#21D376'
            }
          },
          data:data.hy.data
        },
        {
          name: '企业自查',
          type: 'bar',
          itemStyle: {
            normal: {
              color: '#0099FF'
            }
          },
          data: null
        },
        {
          name: '网络排查',
           type: 'bar',
          itemStyle: {
            normal: {
              color: '#F0AC1E'
            }
          },
          data: null
        },
        {
          name: '中介检查',
           type: 'bar',
          itemStyle: {
            normal: {
              color: '#A4BCC6'
            }
          },
          data: undefined
        }
      ],
      xAxis: [
        {
          data: data.hy.x
        }
      ]
    },
      {
        legend: {
        show:true,
          
          data: ['政府排查', '企业自查', '网络排查','中介检查'],
         
        },
        series:[
          {
            name: '政府排查',
            type: 'bar',
            barWidth: '',
            itemStyle: {
              normal: {
                color: '#EB3432'
              }
            },
            data: data.dq.data[0]
          },
          {
            name: '企业自查',
            type: 'bar',
            itemStyle: {
              normal: {
                color: '#0099FF'
              }
            },
            data: data.dq.data[1]
          },
          {
            name: '网络排查',
            type: 'bar',
            itemStyle: {
              normal: {
                color: '#F0AC1E'
              }
            },
            data: data.dq.data[2]
          },
          {
            name: '中介检查',
            type: 'bar',
            itemStyle: {
              normal: {
                color: '#A4BCC6'
              }
            },
            data: data.dq.data[3]
          }

        ],
        xAxis: [
          {
            data: data.dq.x
          }
        ]
      },

    ]
  }

  chart.setOption(option)

  return chart;

}

//隐患类别top10
function showBar3 (data) {
  var chart = echarts.init(document.getElementById('bar3'))
  chart.setOption(
    {
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        top: '15%',
        left: '3%',
        right: '10%',
        bottom: '5%',
        containLabel: true
      },

      xAxis: [
        {
          type: 'value',

          splitLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisLine: {
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            interval: 0,
            textStyle: {
              color: '#97B8C9'

            }
          }
        }
      ],
      yAxis: [
        {
          type: 'category',
          inverse: true,
          data: data.y,
          axisLabel : {
            textStyle: {
              color: '#97B8C9'
            },
            interval:0,
            formatter:function (val) {
              if  (val.length >= 6){
                return val.substring(0,6) + '...'
              }
              return val;
            }
          },
          axisLine: {
            show: true,
            lineStyle: {
              color: '#07274D'
            }
          },
          axisTick: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          splitLine: {
            show: false
          }
        }
      ],
      series: [
        {
          name: '隐患类别TOP10分布情况',
          type: 'bar',
          barWidth: '50%',
          data: data.data,
          label:{
            normal:{
              show:true,
              position:'right',
              color:'#97B8C9'
            }
          }
        }


      ]

    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart
}

function showMap (mapdata, max, code) {
  var chart = echarts.init(document.getElementById('map'))
  $.get('./map/' + code + '.json', function (json) {

    echarts.registerMap(code, json)
    var option = {
      tooltip: {
        trigger: 'item'
      },
      visualMap: {
        min: 0,
        max: max,
        left: '20',
        top: 'bottom',
        textStyle: {
          color: '#4BCEEC'
        },
        text: ['高', '低'],           // 文本，默认为数值文本
        color: [
          '#284CA9',
          '#4DD5EF'],
        calculable: true
      },
      series: [
        {
          name: '企业自查自报隐患数',
          roam: true,
          type: 'map',
          zoom:1.2,
          mapType: code,
          label: {
            normal: {
              show: true,
              color: '#062E4C'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: 1.5,
            }
          },
          data: mapdata
        }
      ]
    }

    chart.setOption(option)
  })
  return chart
}